<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\BlogController;
use App\Http\Controllers\Api\FaqController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/* blog api's */
Route::get('get-blogs',[BlogController::class , 'getBlogs']);
Route::get('get-blog-detail/{id}',[BlogController::class , 'getBlogDetail']);

/* Faq's api */
Route::get('get-faqs',[FaqController::class , 'getFaq']);

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
