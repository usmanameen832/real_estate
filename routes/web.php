<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SuperAdmin\BlogController;
use App\Http\Controllers\SuperAdmin\FaqController;
use App\Http\Controllers\SuperAdmin\ProfileController;
use App\Http\Controllers\SuperAdmin\AuthController;
use App\Http\Controllers\SuperAdmin\DocumentController;
use App\Http\Controllers\SuperAdmin\ChecklistController;
use App\Http\Controllers\SuperAdmin\AgentsController;
use App\Http\Controllers\SuperAdmin\TransactionsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//Auth::routes();

//Route::view('/welcome', 'welcome');

Route::get('/', function () {
   return redirect('/superadmin/login');
});

Route::prefix('superadmin')->middleware('cached')->group(function () {
    Route::middleware('check_admin')->group(function () {
        Route::get('login',[AuthController::class , 'showLoginForm'])->name('showLoginForm');
        Route::post('authenticate',[AuthController::class , 'loginPost'])->name('postLogin');
    });

    Route::middleware('super_admin')->group(function () {
        /* super admin routes */
        Route::get('/home', function () { return view('superadmin.index'); });

        /* setup checklist */
        Route::get('/checklist', [ChecklistController::class, 'checkList'])->name('checklist');

        /* Agents Routes */
        Route::get('/agent', [AgentsController::class, 'agent'])->name('agent');
        Route::get('/add-agent', [AgentsController::class, 'addAgent'])->name('add-agent');
        Route::get('/office-license', [AgentsController::class, 'officeLicense'])->name('office-license');
        Route::get('/agent-billing', [AgentsController::class, 'agentBilling'])->name('billing');
        Route::get('/other-income-log', [AgentsController::class, 'otherIncome'])->name('other-income');
        Route::get('/office-documents', [AgentsController::class, 'officeDoc'])->name('office-doc');
        Route::get('/onboarding-queue', [AgentsController::class, 'onboarding'])->name('onboarding');

        /* Transactions Routes */
        Route::get('/view-transactions', [TransactionsController::class, 'viewTransaction'])->name('view');

        /* blog routes */
        Route::get('add-blog',[BlogController::class , 'addBlog'])->name('addBlog');
        Route::post('post-blog',[BlogController::class , 'blogPost'])->name('blogPost');
        Route::post('update-blog-post/{id}',[BlogController::class , 'updateBlog'])->name('updateBlog');
        Route::get('blogs',[BlogController::class , 'allBlogs'])->name('allBlogs');
        Route::get('edit-Blog/{id}',[BlogController::class , 'editBlog'])->name('editBlog');
        Route::get('delete-Blog/{id}',[BlogController::class , 'deleteBlog'])->name('deleteBlog');

        /* faq's routes */
        Route::get('add-faq',[FaqController::class , 'index'])->name('addFaq');
        Route::get('faq',[FaqController::class , 'allFaq'])->name('allFaqs');
        Route::post('post-faq',[FaqController::class , 'faqPost'])->name('faqPost');
        Route::get('edit-faq/{id}',[FaqController::class , 'editFaq'])->name('editFaq');
        Route::get('delete-faq/{id}',[FaqController::class , 'deleteFaq'])->name('deleteFaq');
        Route::post('update-faq/{id}',[FaqController::class , 'updateFaq'])->name('updateFaq');

        /* admin profile routes */
        Route::get('profile',[ProfileController::class , 'profile'])->name('profile');
        Route::post('update-profile',[ProfileController::class , 'updateProfile'])->name('updateProfile');

        /* document's routes */
        Route::get('upload-document',[DocumentController::class , 'uploadDocument'])->name('uploadDocument');
        Route::get('delete-document/{id}',[DocumentController::class , 'deleteDocument'])->name('deleteDocument');
        Route::post('post-upload-document',[DocumentController::class , 'postUploadDocument'])->name('postUploadDocument');

    });

    Route::get('logout-admin',[AuthController::class , 'logoutAdmin'])->name('logoutAdmin');
});
