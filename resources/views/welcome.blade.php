
{{--<!DOCTYPE html>--}}
{{--<!--[if IE 9 ]> <html class="ie9"> <![endif]-->--}}
{{--<html>--}}
{{--<head>--}}
{{--    <meta charset="UTF-8">--}}
{{--    <title>Real Estate Back Office Software, Accounting</title>--}}

{{--    <meta name="google-site-verification" content="Y52RJwo8FuVAsS3pNz_BHoMO7t1nujSKd9oL93LxoF4" />--}}
{{--    <meta http-equiv="X-UA-Compatible" content="IE=edge">--}}
{{--    <meta name="msvalidate.01" content="6E9242C7D9D3F7CFA0E28EB1C7408416" />--}}
{{--    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>--}}
{{--    <link href="https://brokersumo.com/css/jquery.bootstrap.css" rel="stylesheet" type="text/css" />--}}
{{--    <!-- <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" /> -->--}}
{{--    <link href="https://brokersumo.com/css/font-awesome-4.3.0/css/font-awesome.css" rel="stylesheet" type="text/css" />--}}
{{--    <link href="https://brokersumo.com/css/bootstrap-tagsinput/bootstrap-tagsinput.css" rel="stylesheet" type="text/css" />--}}
{{--    <link href="https://brokersumo.com/css/jquery.chosen.css" rel="stylesheet" type="text/css" />--}}
{{--    <link href="https://brokersumo.com/css/jquery.icheck-blue.css" rel="stylesheet" type="text/css" />--}}
{{--    <!-- <link href="https://brokersumo.com/css/jquery.daterangepicker.css" rel="stylesheet" type="text/css" /> -->--}}
{{--    <link href="https://brokersumo.com/css/daterangepicker.css" rel="stylesheet" type="text/css" />--}}
{{--    <link href="https://brokersumo.com/css/jquery.ui.css" rel="stylesheet" type="text/css" />--}}
{{--    <link href="https://brokersumo.com/css/jquery.fancytree.css" rel="stylesheet" type="text/css" />--}}
{{--    <link href="https://brokersumo.com/css/bootstrap-switch.css" rel="stylesheet" type="text/css" />--}}
{{--    <link href="https://brokersumo.com/css/jquery.fileupload.css" rel="stylesheet" type="text/css" />--}}
{{--    <link href="https://brokersumo.com/css/jquery.skin-red.css" rel="stylesheet" type="text/css" />--}}
{{--    <link href="https://brokersumo.com/css/jquery.qtip.css" rel="stylesheet" type="text/css" />--}}
{{--    <link href="https://brokersumo.com/css/main.css" rel="stylesheet" type="text/css" />--}}
{{--    <link href="https://brokersumo.com/css/styleSheet.css" rel="stylesheet" type="text/css" />--}}
{{--    <link href="https://brokersumo.com/css/color-picker/color-picker.min.css" rel="stylesheet" type="text/css" />--}}
{{--    <!-- full Calendar 3.4.0 -->--}}
{{--    <link href="css/fullcalendar-3.4.0/fullcalendar.min.css" rel="stylesheet" type="text/css" />--}}

{{--    <!--   <link href="css/fullcalendar-3.4.0/fullcalendar.print.min.css" rel="stylesheet" type="text/css" /> -->--}}

{{--    <!-- favicon -->--}}
{{--    <link rel="icon" href="pages/img/logos/favicon.png">--}}

{{--    <script src="https://brokersumo.com/js/jquery.js" type="text/javascript" ></script>--}}
{{--    <script src="https://brokersumo.com/js/external/angularjs/angular.min.js" type="text/javascript"></script>--}}
{{--    <script src="https://brokersumo.com/js/external/angularjs/angular-resource.js" type="text/javascript"></script>--}}
{{--    <script src="https://brokersumo.com/js/external/angularjs/angular-route.js" type="text/javascript"></script>--}}

{{--    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->--}}
{{--    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->--}}
{{--    <!--[if lt IE 9]>--}}
{{--    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>--}}
{{--    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>--}}
{{--    <![endif]-->--}}

{{--    <script type="text/javascript">--}}
{{--        var _mfq = _mfq || [];--}}
{{--        (function() {--}}
{{--            var mf = document.createElement("script");--}}
{{--            mf.type = "text/javascript"; mf.async = true;--}}
{{--            mf.src = "//cdn.mouseflow.com/projects/d16acb57-27d5-4604-a00e-23b76cb6fe3d.js";--}}
{{--            document.getElementsByTagName("head")[0].appendChild(mf);--}}
{{--        })();--}}
{{--    </script>--}}

{{--    <script>--}}
{{--        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){--}}
{{--            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),--}}
{{--            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)--}}
{{--        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');--}}

{{--        ga('create', 'UA-69841009-2', 'auto');--}}
{{--        ga('send', 'pageview');--}}

{{--    </script>--}}



{{--</head>--}}
{{--<body class="skin-red" data-ng-app="brokersumo-app" data-ng-class="authenticate ? 'sidebar-mini' : 'register-page'" data-ng-cloak="" ng-init="getIncludeExcludeForIndexPage();">--}}
{{--<div class="" data-ng-class="authenticate ? 'wrapper' : ''">--}}

{{--    <div class="loadingModal modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" style="z-index:1111">--}}
{{--        <div class="modal-dialog modal-sm">--}}
{{--            <div class="modal-content">--}}
{{--                <img src="pages/img/ring.gif"/>--}}
{{--            </div>--}}
{{--            <div class="modal-footer" style="border:0px;">--}}
{{--                <button class="btn btn-danger" data-ng-click="cancelLoadingModal();" >Close</button>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}

{{--    <header class="main-header" data-ng-if="ShowHeader">--}}
{{--        <!-- Logo -->--}}
{{--        <a data-ng-show="user.isBranding===true || user.isBranding===false" href="#" class="logo" style = "background-color: {{user.isBranding ? user.brandHexCode : '#d73925'}} !important">--}}
{{--            <!-- mini logo for sidebar mini 50x50 pixels -->--}}
{{--            <span data-ng-if="user.isBranding === false" class="logo-mini"><img src="pages/img/logos/small_logo.png" /></span>--}}
{{--            <span data-ng-if="user.isBranding" class="logo-mini"><img width="45" height="45" data-ng-src="{{user.brandSmallLogoUrl || 'pages/img/logos/small_logo.png'}}"/></span>--}}
{{--            <!-- logo for regular state and mobile devices -->--}}
{{--            <span data-ng-if="user.isBranding === false" class="logo-lg"><img src="pages/img/logos/big_logo.png" /></span>--}}
{{--            <span data-ng-if="user.isBranding" class="logo-lg"><img width="187" height="45" data-ng-src="{{user.brandLogoUrl || 'pages/img/logos/big_logo.png'}}" /></span>--}}
{{--        </a>--}}
{{--        <nav data-ng-show="user.isBranding===true || user.isBranding===false" class="navbar navbar-static-top" role="navigation" style = "background-color: {{user.isBranding ? user.brandHexCode : '#dd4b39'}} !important">--}}
{{--            <!-- Sidebar toggle button-->--}}
{{--            <a class="sidebar-toggle" style="{{setHeaderView(user)}}"--}}
{{--               data-toggle="offcanvas" role="button" data-ng-show="(authenticate==true&&user.type=='broker')||user.loggedIn=='2' || user.isPlaidAgent">--}}
{{--                <span class="sr-only">Toggle navigation</span>--}}
{{--            </a>--}}
{{--            <div class="navbar-custom-menu" data-ng-show="authenticate">--}}
{{--                <ul class="nav navbar-nav">--}}
{{--                    <li class="dropdown user user-menu">--}}
{{--                        <a href="" class="dropdown-toggle" data-toggle="dropdown" style="cursor:default;{{setHeaderView(user)}}" data-ng-click = "switchAgentOffice()">--}}
{{--                            <!-- <span class="user-image"><i class="fa fa-user"></i></span> -->--}}
{{--                            <span class="hidden-xs" data-ng-bind="user.email"></span>--}}
{{--                            <span data-ng-if = "(user.officeName !=null) || (user.officeName !=undefined)" data-ng-bind = "'('+ user.officeName + ')'"></span>--}}
{{--                        </a>--}}
{{--                    </li>--}}
{{--                    <li>--}}
{{--                        <a data-ng-click="logoutUser();" style="cursor: pointer;{{setHeaderView(user)}}">Logout</a>--}}
{{--                    </li>--}}
{{--                </ul>--}}
{{--            </div>--}}
{{--        </nav>--}}
{{--    </header>--}}

{{--    <div data-common-alert-message=""></div>--}}

{{--    <!-- Left side column. contains the logo and sidebar -->--}}
{{--    <aside class="main-sidebar" data-ng-show="(authenticate==true && ((user.type=='broker' && (user.brokerType == 2 || user.superAdmin)) || (user.brokerType == 3 && !user.agentAccessOnly && !user.transactionAccessOnly && !user.reportAccessOnly)) )||(user.loggedIn=='2' && user.accessAgentPortal)||user.isPlaidAgent" data-ng-if="ShowHeader">--}}
{{--        <section class="sidebar">--}}
{{--            <ul class="sidebar-menu" data-ng-show="user.isPlaidAgent">--}}
{{--                <li class="treeview">--}}
{{--                    <a href="/#/plaid-dashboard">--}}
{{--                        <i class="fa fa-dashboard"></i> <span>Dashboard</span>--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--                <li class="treeview">--}}
{{--                    <a href="">--}}
{{--                        <i class="fa fa-exchange"></i><span>Transactions</span>--}}
{{--                        <i class="fa fa-angle-left pull-right"></i>--}}
{{--                    </a>--}}
{{--                    <ul class="treeview-menu">--}}
{{--                        <li><a href="" data-ng-click="viewPlaidTransaction();"><i class="fa fa-circle-o"></i> View Transactions</a></li>--}}
{{--                        <li><a href="/#/add-plaid-transaction"><i class="fa fa-plus"></i> Add Transaction</a></li>--}}
{{--                    </ul>--}}
{{--                </li>--}}
{{--                <li class="treeview">--}}
{{--                    <a href="">--}}
{{--                        <i class="fa fa-dollar"></i><span>Money</span>--}}
{{--                        <i class="fa fa-angle-left pull-right"></i>--}}
{{--                    </a>--}}
{{--                    <ul class="treeview-menu">--}}
{{--                        <li class="treeview">--}}
{{--                            <a href="/#/pay-vendors">--}}
{{--                                <i class="fa fa-money"></i> <span> Send Money</span>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                        <!-- <li data-ng-show="user.brokerType != 3 || user.superAdmin">--}}
{{--                           <a href="/#/plaid-transaction">--}}
{{--                               <i class="fa fa-circle-o"></i> <span>Banking</span>--}}
{{--                           </a>--}}
{{--                       </li> -->--}}
{{--                        <li data-ng-show="user.type=='broker' && (user.brokerType==2 || user.brokerType==3)">--}}
{{--                            <a href="/#/agent-1099s">--}}
{{--                                <i class="fa fa-circle-o"></i> 1099s--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                    </ul>--}}
{{--                </li>--}}
{{--                <li class="treeview">--}}
{{--                    <a href="/#/reports">--}}
{{--                        <i class="fa fa-pie-chart"></i> <span> Reports</span>--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--                <li class="treeview">--}}
{{--                    <a href="">--}}
{{--                        <i class="fa fa-user"></i> <span>My Account </span>--}}
{{--                        <i class="fa fa-exclamation red" data-ng-if="!user.bundle && user.creditCardStatus == 0"></i>--}}
{{--                        <i class="fa fa-angle-left pull-right"></i>--}}
{{--                    </a>--}}
{{--                    <ul class="treeview-menu">--}}
{{--                        <li><a href="/#/plaid-agent-account-info"><i class="fa fa-circle-o"></i> Account Settings</a></li>--}}
{{--                        <li data-ng-if="user.enableBillingInfo && (user.brokerType==2 || user.superAdmin)"><a href="/#/plaid-billing-info"><i class="fa fa-circle-o"></i> Billing Info--}}
{{--                                <small class="label bg-red" data-ng-if="!user.bundle && user.creditCardStatus == 0 && user.trialExpiresIn >= 0">{{user.trialExpiresIn}} Days Left</small>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                    </ul>--}}
{{--                </li>--}}

{{--                <li class="treeview">--}}
{{--                    <a href="">--}}
{{--                        <i class="fa fa-cog"></i> <span>Settings</span>--}}
{{--                        <i class="fa fa-angle-left pull-right"></i>--}}
{{--                    </a>--}}
{{--                    <ul class="treeview-menu">--}}
{{--                        <li><a href="/#/bank-accounts"><i class="fa fa-circle-o"></i> Bank Accounts</a></li>--}}
{{--                        <li><a href="/#/manage-tags"><i class="fa fa-circle-o"></i> Tags</a></li>--}}
{{--                        <li>--}}
{{--                            <a href="" data-ng-click="viewVendors()">--}}
{{--                                <i class="fa fa-circle-o"></i> <span>Vendors</span>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                        <!--<li data-ng-if="!user.newDwolla"><a href="/#/direct-deposit"><i class="fa fa-circle-o"></i> Payments</a></li>-->--}}
{{--                    </ul>--}}
{{--                </li>--}}
{{--                <li>--}}
{{--                    <a href="">--}}
{{--                        <i class="fa fa-question"></i> <span>Help</span>--}}
{{--                        <i class="fa fa-angle-left pull-right"></i>--}}
{{--                    </a>--}}
{{--                    <ul class="treeview-menu">--}}
{{--                        <!-- BROK-3129 -->--}}
{{--                        <!-- <li><a href="https://www.youtube.com/c/brokersumorealestate" target="_blank"><i class="fa fa-video-camera"></i> Videos</a></li> -->--}}
{{--                        <li><a href="/cdn-cgi/l/email-protection#7c1e0e1317190e0f0911133c15120f1518190e191d10190f081d0819521f1311" target="_blank"><i class="fa  fa-envelope-o"></i> Email Support</a></li>--}}
{{--                        <li><a href="https://help.insiderealestate.com/en/collections/1898269-core-back-office" target="_blank"><i class="fa fa-lightbulb-o"></i> Support Center</a></li>--}}
{{--                        <li><a href="https://insiderealestate.thinkific.com/api/sso/v2/sso/jwt?jwt={{ user.thinkificToken }}&return_to=https%3A%2F%2Flearn.insiderealestate.com/" target="_blank"><img height="15" src="pages/img/learning-bs.png" /> Learning Portal</a></li>--}}
{{--                    </ul>--}}
{{--                </li>--}}
{{--            </ul>--}}
{{--            <ul class="sidebar-menu" data-ng-show="!user.isPlaidAgent">--}}
{{--                <li class="treeview">--}}
{{--                    <a href="/#/dashboard">--}}
{{--                        <i class="fa fa-dashboard"></i> <span>Dashboard</span>--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--                <li class="treeview" data-ng-show="user.type=='broker' && (user.brokerType==2 || user.superAdmin) && user.isShowSetupChecklist">--}}
{{--                    <a href="/#/setup-checklist">--}}
{{--                        <i class="fa fa-dashboard"></i> <span>Setup Checklist</span>--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--                <li class="treeview" data-ng-show="user.isProAccount && user.type == 'broker'">--}}
{{--                    <a href="/#/calendar">--}}
{{--                        <i class="fa fa-calendar"></i> <span>Calendar</span>--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--                <li class="treeview" data-ng-show="user.type=='broker'">--}}
{{--                    <a href="">--}}
{{--                        <i class="fa fa-users"></i>--}}
{{--                        <span>Agents</span>--}}
{{--                        <i class="fa fa-angle-left pull-right"></i>--}}
{{--                    </a>--}}
{{--                    <ul class="treeview-menu">--}}
{{--                        <li class="active"><a href="" data-ng-click="viewAgent();"><i class="fa fa-circle-o"></i>Agents ({{user.agentCount}})</a></li>--}}
{{--                    <!-- <li><a href="/#/agent-view" ><i class="fa fa-circle-o"></i> New Agents ()</a></li> -->--}}
{{--                        <li><a href="" data-ng-click="addAgent();"><i class="fa fa-circle-o"></i>+ Add Agents</a></li>--}}
{{--                        <li><a href="/#/manage-agent-office-license"><i class="fa fa-circle-o"></i>+ Add Agents Office/License</a></li>--}}
{{--                        <li><a href="/#/agent-billing"><i class="fa fa-circle-o"></i>Agent Billing</a></li>--}}
{{--                        <li><a href="/#/other-income-log"><i class="fa fa-circle-o"></i>Other Income Log</a></li>--}}
{{--                        <li class="treeview">--}}
{{--                            <a href="/#/office-documents"><i class="fa fa-file" aria-hidden="true"></i><span> Office Documents</span></a></li>--}}
{{--                        <li><a href="" data-ng-if="user.isProAccount" data-ng-click="teams();"><i class="fa fa-circle-o"></i>Teams</a></li>--}}
{{--                        <li data-ng-if="user.enableImportBilling"><a href="/#/import-billing"><i class="fa fa-circle-o"></i>Import Agent Billing</a></li>--}}
{{--                        <li><a href="/#/onboarding-queue"><i class="fa fa-circle-o"></i>Onboarding Queue</a></li>--}}
{{--                        <li data-ng-if="user.firstTeamCompany || user.companyId == 5 || user.companyId == 314"><a href="/#/import-first-team-custom-fields"><i class="fa fa-circle-o"></i>Import FT Custom fields</a></li>--}}
{{--                    </ul>--}}
{{--                </li>--}}
{{--                <li class="treeview">--}}
{{--                    <a href="">--}}
{{--                        <i class="fa fa-exchange"></i><span>Transactions</span>--}}
{{--                        <i class="fa fa-angle-left pull-right"></i>--}}
{{--                    </a>--}}
{{--                    <ul class="treeview-menu">--}}
{{--                        <li><a href="" data-ng-click="viewTransaction();"><i class="fa fa-circle-o"></i> View Transactions</a></li>--}}
{{--                        <li data-ng-show="user.type=='broker' || (user.type=='agent' && (user.isAllowTransactionForAgent || user.isAgentAllowTransaction))"><a href="/#/add-transaction"><i class="fa fa-plus"></i> Add Transaction</a></li>--}}
{{--                        <li data-ng-show="user.type=='broker'"><a data-ng-click="addDemand();"><i class="fa fa-plus"></i> Create Disbursement</a></li>--}}
{{--                        <!-- <li data-ng-show="user.type=='broker' && (user.userId == 315 || user.userId == 6)"><a data-ng-click="newAddDemand();"><i class="fa fa-plus"></i> New Create Disbursement</a></li> -->--}}
{{--                        <!-- (user.type=='agent' && ((user.isAllowTransactionForAgent && user.isAllowAgentForCreateDisbursement) || (user.isAgentAllowTransaction && user.isAgentAllowForCreateDisbursement))) -->--}}
{{--                        <!-- <li><a href="/#/pre-demand"><i class="fa fa-circle-o"></i> Pre-Disbursement</a></li> -->--}}
{{--                        <li><a href="/#/trust-account-deposits"><i class="fa fa-circle-o"></i> Trust Account / Deposits</a></li>--}}
{{--                        <li data-ng-show="user.type=='agent'"><a href="/#/agent-commissions"><i class="fa fa-money"></i> Commissions</a></li>--}}
{{--                        <li data-ng-show="(user.type=='broker' && (user.dotLoopSetUp || user.isCompanyDotloopSetUp)) || (user.type=='agent' && user.dotLoopSetUp && (user.companyId == 1177994 || user.companyId === 5 || user.companyId == 314))"><a href="" data-ng-click="dotloopSyncUrl();"><i class="fa fa-circle-o"></i> Dotloop</a></li>--}}
{{--                        <li data-ng-show="user.type=='broker' && user.skySlopeSetup"><a href="/#/broker-sky-slope"><i class="fa fa-circle-o"></i> SkySlope</a></li>--}}
{{--                        <li data-ng-show="user.type=='agent' && user.allowAgentSkyslopeLog"><a href="/#/agent-sky-slope-logs"><i class="fa fa-circle-o"></i> Skyslope Logs</a></li>--}}
{{--                        <li data-ng-show="user.type=='agent' && user.allowAgentDotloopLog"><a href="/#/agent-dotloop-logs"><i class="fa fa-circle-o"></i> Dotloop Logs</a></li>--}}
{{--                        <li data-ng-show="user.type=='broker' && user.isProAccount"><a href="/#/mls-integration"><i class="fa fa-circle-o"></i> MLS</a></li>--}}
{{--                        <li data-ng-show="user.isDocusignTransaction"><a href="/#/docusign-transaction-log"><i class="fa fa-circle-o"></i> Docusign Transaction Log</a></li><!--(user.companyId == 5 || user.companyId == 314 || user.companyId == 3841048 || user.companyId == 1048405 || user.companyId == 3360014 || user.companyId == 4880347)-->--}}
{{--                        <li data-ng-show="user.companyId == 41089921"><a href="/#/import-companies"><i class="fa fa-upload"></i>Import Companies</a></li>--}}
{{--                        <li data-ng-show="user.zohoSync && user.type=='broker'"><a href="/#/zoho-bulk-sync"><i class="fa fa-circle-o"></i>Zoho Bulk Sync</a></li>--}}
{{--                    </ul>--}}
{{--                </li>--}}
{{--                <li class="treeview">--}}
{{--                    <a href="" data-ng-show="(user.type=='broker' && (user.brokerType == 2 || user.superAdmin || (user.brokerType == 3 && user.isStaffNoLimit && !user.isStaffRestrictedToTeamOnly))) || (user.type !='broker' && user.isPlaidBroker)">--}}
{{--                        <i class="fa fa-dollar"></i><span>Money</span>--}}
{{--                        <i class="fa fa-angle-left pull-right"></i>--}}
{{--                    </a>--}}
{{--                    <ul class="treeview-menu">--}}
{{--                        <li class="treeview" data-ng-show="!user.newDwolla && ((user.type=='agent'  && user.isPlaidBroker) || (user.type=='broker' && (user.brokerType != 3 || user.superAdmin || (user.brokerType == 3 && user.isAllowStaffUserDirectDeposit))))">--}}
{{--                            <a href="/#/pay-vendors">--}}
{{--                                <i class="fa fa-money" data-ng-if="user.type=='agent'"></i>--}}
{{--                                <span data-ng-if="user.type=='agent'"> Send Money</span>--}}
{{--                                <!--<span data-ng-if="user.type=='broker'"> Pay Vendors</span>-->--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                        <!-- <li data-ng-show="((user.isQbIntegrated || user.isPlaidBroker) && (user.brokerType != 3 || user.superAdmin))">--}}
{{--                            <a href="/#/plaid-transaction">--}}
{{--                                <i class="fa fa-circle-o"></i> <span>Banking</span>--}}
{{--                            </a>--}}
{{--                        </li> -->--}}
{{--                        <!--  	<li data-ng-show="user.type=='broker' && (user.brokerType==2 || user.superAdmin)"><a href="/#/export-data"><i class="fa fa-download"></i> Export Data</a></li> -->--}}
{{--                        <!-- <li data-ng-show="user.type=='broker' && (user.companyId==314 || user.userId==330549 || user.userId==495768 || user.userId==520615)"><a href="/#/new-export-data"><i class="fa fa-download"></i> New Export Data</a></li> -->--}}
{{--                        <li data-ng-show="user.type=='broker' && (user.brokerType==2 || user.brokerType==3)">--}}
{{--                            <a href="/#/agent-1099s">--}}
{{--                                <i class="fa fa-circle-o"></i> 1099 Exports--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                        <li data-ng-show="user.type=='broker' && (user.brokerType==2 || user.brokerType==3) && user.tax1099Auth">--}}
{{--                            <a href="/#/tax-1099-integration">--}}
{{--                                <i class="fa fa-circle-o"></i> Tax1099 Integration--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                        <!--<li data-ng-show="user.type=='broker'">--><!--  && user.brokerType==2 -->--}}
{{--                        <!---	<a href="/#/payment-log">--}}
{{--                                <i class="fa fa-circle-o"></i> Payment Log--}}
{{--                            </a>--}}
{{--                        </li>-->--}}
{{--                        <li data-ng-if="user.type=='broker' && (user.brokerType==2 || user.superAdmin) && user.isQbIntegrated">--}}
{{--                            <a href="" data-ng-click="qbIntegration();">--}}
{{--                                <i class="fa fa-circle-o"></i> QuickBooks Integration--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                    </ul>--}}
{{--                </li>--}}
{{--                <li class="treeview">--}}
{{--                    <a href="/#/reports">--}}
{{--                        <i class="fa fa-pie-chart"></i> <span> Reports</span>--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--                <li class="treeview" data-ng-if="user.type=='agent'">--}}
{{--                    <a href="/#/office-documents">--}}
{{--                        <i class="fa fa-file" aria-hidden="true"></i>--}}
{{--                        <span> Office Documents</span>--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--                <li class="treeview" data-ng-show="user.type=='broker'">--}}
{{--                    <a href="">--}}
{{--                        <i class="fa fa-user"></i> <span>My Account </span>--}}
{{--                        <i class="fa fa-exclamation red" data-ng-if="!user.bundle && user.creditCardStatus == 0 && (user.brokerType==2 || user.superAdmin)"></i>--}}
{{--                        <i class="fa fa-angle-left pull-right"></i>--}}
{{--                    </a>--}}
{{--                    <ul class="treeview-menu">--}}
{{--                        <li data-ng-if="user.brokerType==2 || user.superAdmin"><a href="/#/broker-account-info"><i class="fa fa-circle-o"></i> Account Settings</a></li>--}}
{{--                        <li data-ng-if="!user.isStaffRestrictedToTeamOnly && user.enableBillingInfo && (user.brokerType==2 || user.superAdmin)" ><a href="/#/broker-billing-info"><i class="fa fa-circle-o"></i> Billing Info--}}
{{--                                <small class="label bg-red" data-ng-if="!user.bundle && user.creditCardStatus == 0 && user.trialExpiresIn >= 0">{{user.trialExpiresIn}} Days Left</small>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                        <li data-ng-if="user.brokerType==2 || user.superAdmin"><a href="/#/staff-users"><i class="fa fa-circle-o"></i> Staff Users</a></li>--}}
{{--                        <li data-ng-if="user.brokerType==3 || user.superAdmin"><a href="/#/staff-user-account-info"><i class="fa fa-key"></i> Reset Password</a></li>--}}
{{--                    </ul>--}}
{{--                </li>--}}
{{--                <li class="treeview" data-ng-show="user.type=='broker' && (user.brokerType == 2 || user.superAdmin || (user.brokerType == 3 && !user.isStaffRestrictedToTeamOnly))">--}}
{{--                    <a href="">--}}
{{--                        <i class="fa fa-cog"></i> <span>Settings</span>--}}
{{--                        <i class="fa fa-angle-left pull-right"></i>--}}
{{--                    </a>--}}
{{--                    <ul class="treeview-menu">--}}
{{--                        <li data-ng-if="user.type=='broker' && user.isProAccount"><a href="/#/auto-fill-co-broke"><i class="fa fa-circle-o"></i> Add/Edit Co-Broke</a></li>--}}
{{--                        <!-- <li data-ng-if="user.brokerType==2 || user.superAdmin"><a href="/#/auto-fill-co-broke"><i class="fa fa-circle-o"></i> Edit Co-Broke</a></li> -->--}}
{{--                        <li data-ng-if="user.brokerType==2"><a href="/#/credit-card-setting"><i class="fa fa-circle-o"></i> Agent Billings</a></li>--}}
{{--                        <li><a data-ng-click="agentChecklist()"><i class="fa fa-circle-o"></i> Agent Checklist</a></li>--}}
{{--                        <li data-ng-if="(user.brokerType==2 || user.superAdmin) && user.isProAccount"><a href="/#/agent-profile-master-data"><i class="fa fa-circle-o"></i> Agent Profile Master Data</a></li>--}}
{{--                        <li data-ng-show="user.isPlaidBroker && (user.brokerType==2 || user.superAdmin)"><a href="/#/bank-accounts"><i class="fa fa-circle-o"></i> Bank Accounts</a></li>--}}
{{--                        <li data-ng-if="user.bankDepositInfo && (user.brokerType==2 || user.superAdmin)"><a href="/#/bank-deposit"><i class="fa fa-circle-o"></i> Bank Deposit Info</a></li>--}}
{{--                        <li><a href="" id="commission-plan" data-ng-click="commissionPlan();"><i class="fa fa-circle-o"></i> Commission Plans</a></li>--}}
{{--                        <li data-ng-if="user.commissionPlanFee"><a href="/#/commission-plan-fees"><i class="fa fa-circle-o"></i>Commission Plan Fees</a></li>--}}
{{--                        <li><a href="/#/edit-title-attorneys"><i class="fa fa-circle-o"></i> Closing Vendors</a></li>--}}
{{--                        <li data-ng-if="user.brokerType == 2 || user.superAdmin"><a href="/#/default-state"><i class="fa fa-circle-o"></i> Default State</a></li>--}}
{{--                        <li data-ng-if="user.brokerType==2 || user.superAdmin"><a href="/#/auto-fill-items"><i class="fa fa-circle-o"></i> Edit Fees</a></li>--}}
{{--                        <li><a href="/#/field-validation"><i class="fa fa-circle-o"></i> Field Validation</a></li>--}}
{{--                        <li data-ng-if="user.brokerType==2 || user.superAdmin"><a href="" data-ng-click="integrationUrl();"><i class="fa fa-circle-o"></i> Integrations</a></li>--}}
{{--                        <li data-ng-if="(user.brokerType==2 || user.superAdmin) && user.companyId != 314"><a href="/#/import-agent-billing-data"><i class="fa fa-circle-o"></i> Import Agent Billing Data</a></li>--}}
{{--                        <li data-ng-if="(user.brokerType==2 || user.superAdmin) && user.isProAccount"><a href="/#/add-license-types"><i class="fa fa-circle-o"></i> License Types</a></li>--}}
{{--                        <li data-ng-if="user.brokerType==2 && user.companyId ==9203732"><a href="/#/copy-agents"><i class="fa fa-circle-o"></i> Move Agents</a></li>--}}
{{--                        <li><a href="/#/docusign-template"><i class="fa fa-circle-o"></i> Onboarding Templates</a></li><!-- && (user.userId == 6 || user.userId == 315) -->--}}
{{--                        <!-- <li data-ng-if="user.accessAgentPortal && (user.userId == 6 || user.userId == 315)"><a href="/#/broker-template"><i class="fa fa-circle-o"></i> Onboarding Template</a></li> -->--}}
{{--                        <li><a href="/#/organization"><i class="fa fa-circle-o"></i> Organization</a></li>--}}
{{--                        <!--<<li data-ng-if="(user.brokerType==2 || user.superAdmin) && !user.newDwolla"><a href="/#/direct-deposit"><i class="fa fa-circle-o"></i> Payments</a></li>-->--}}
{{--                        <li><a href="/#/agent-permission"><i class="fa fa-key"></i> Permissions</a></li>--}}

{{--                        <li><a href="/#/manage-tags"><i class="fa fa-circle-o"></i> Tags</a></li>--}}
{{--                        <li><a data-ng-click="taskChecklist()"><i class="fa fa-circle-o"></i> Transaction Checklist</a></li>--}}
{{--                        <li data-ng-if="(user.brokerType==2 || user.superAdmin) && user.isProAccount"><a href="/#/transaction-master-data"><i class="fa fa-circle-o"></i> Transaction Master Data</a></li>--}}
{{--                        <li><a href="" data-ng-click="viewVendors()"><i class="fa fa-circle-o"></i> Vendors</a></li>--}}
{{--                        <!-- <li data-ng-if="(user.brokerType==2 || user.superAdmin) && (user.userId == 315 || user.userId == 6)"><a href="/#/auto-fill-credit-debit"><i class="fa fa-circle-o"></i> Credit/Debit</a></li>--}}
{{--                        <li data-ng-if="(user.brokerType==2 || user.superAdmin) && (user.userId == 315 || user.userId == 6)"><a href="/#/auto-fill-billing-item"><i class="fa fa-circle-o"></i> Billing Item</a></li> -->--}}
{{--                        <!-- <li><a href="" data-ng-click="viewTitleCompany()"><i class="fa fa-circle-o"></i> Title Companies</a></li> -->--}}
{{--                        <li data-ng-if="user.companyId === 314 || user.companyId === 24322761 || user.companyId === 5 && (user.brokerType==2 || user.superAdmin)"><a href="/#/dynamics-export"><i class="fa fa-circle-o"></i> Dynamics Export</a></li>--}}
{{--                        <li data-ng-if="user.type == 'broker' && user.isWeichertCompany"><a href="/#/agci-level"><i class="fa fa-circle-o"></i> AGCI Level</a></li>--}}
{{--                        <li data-ng-if="user.agentBillingsSetup"><a href="/#/agent-billings-setup"><i class="fa fa-circle-o"></i> Agent Billings Setup </a></li>--}}
{{--                    </ul>--}}
{{--                </li>--}}
{{--                <li class="treeview" data-ng-show="user.type == 'broker' && (user.brokerType != 3 || (user.brokerType == 3 && user.dwollaCertifyCustomer && user.companyId != 314 && user.companyId != 1677201))  && user.newDwolla">--}}
{{--                    <!-- <li class="treeview" data-ng-show="user.dwollaCertifyCustomer && (user.type == 'broker' && (user.brokerType != 3 || (user.brokerType == 3 && user.dwollaCertifyCustomer && user.companyId != 314 && user.companyId != 1677201)) && user.newDwolla)"> -->--}}
{{--                    <a href="">--}}
{{--                        <i class="fa fa-money cursor"></i> <span> ACH Transfer</span>--}}
{{--                        <i class="fa fa-angle-left pull-right"></i>--}}
{{--                    </a>--}}
{{--                    <ul class="treeview-menu" data-ng-if="user.brokerType == 2">--}}
{{--                        <!-- <li data-ng-if="!user.dwollaCertifyCustomer && !user.dwollaAccessToken"><a href="/#/add-dwolla-customer"><i class="fa fa-plus"></i> Create ACH Transfer Account</a></li> -->--}}
{{--                        <li data-ng-if="!user.dwollaCertifyCustomer && (!user.dwollaAccessToken || user.dwollaCustomerId)"><a href="/#/add-dwolla-customer"><i class="fa fa-plus"></i> Create ACH Transfer Account</a></li>--}}
{{--                        <!-- <li data-ng-if="user.dwollaCertifyCustomer"><a href="/#/dwolla-view-transactions"><i class="fa fa-circle-o"></i> ACH Transfer View Transaction</a></li> -->--}}
{{--                        <li data-ng-if="user.dwollaCertifyCustomer"><a href="/#/add-dwolla-customer-funding-source"><i class="fa fa-circle-o"></i> Bank Accounts</a></li>--}}
{{--                        <li data-ng-if="user.dwollaCertifyCustomer && user.companyId == 314"><a href="/#/pay-staff"><i class="fa fa-circle-o"></i> Pay Staff</a></li>--}}
{{--                        <!-- <li data-ng-if="user.dwollaCertifyCustomer"><a href="/#/dwolla-pay-vendors"><i class="fa fa-circle-o"></i> Pay Vendor</a></li> -->--}}
{{--                        <li data-ng-if="user.dwollaCertifyCustomer"><a href="/#/dwolla-transfers-log"><i class="fa fa-circle-o"></i> Payment Transfer Logs</a></li>--}}
{{--                        <!-- <li data-ng-if="user.dwollaCertifyCustomer"><a href="/#/delete-dwolla-customer"><i class="fa fa-circle-o"></i> Delete ACH Transfer Customer</a></li> -->--}}
{{--                        <li data-ng-if="user.dwollaAccessToken && !user.dwollaCustomerId && !user.dwollaCertifyCustomer"><a href="/#/dwolla-migrate-customer"><i class="fa fa-circle-o"></i> Migrate Customer</a></li>--}}
{{--                    </ul>--}}
{{--                    <ul class="treeview-menu" data-ng-if="user.brokerType == 3">--}}
{{--                        <li><a href="/#/dwolla-transfers-log"><i class="fa fa-circle-o"></i> Payment Transfer Logs</a></li>--}}
{{--                    </ul>--}}
{{--                </li>--}}
{{--                <li class="treeview" data-ng-show="user.type == 'broker' && user.brokerType == 3 && user.dwollaCertifyCustomer && user.newDwolla && (user.companyId == 314 || user.companyId == 1677201)">--}}
{{--                    <a href="">--}}
{{--                        <i class="fa fa-money cursor"></i> <span> ACH Transfer</span>--}}
{{--                        <!-- <i class="fa fa-money cursor"></i> <span> Payments</span> -->--}}
{{--                        <i class="fa fa-angle-left pull-right"></i>--}}
{{--                    </a>--}}
{{--                    <ul class="treeview-menu">--}}
{{--                        <li data-ng-if="!user.staffDwollaCertifyCustomer"><a href="/#/add-dwolla-receive-only-customer"><i class="fa fa-plus"></i> Create ACH Transfer Account</a></li>--}}
{{--                        <li data-ng-if="user.staffDwollaCertifyCustomer"><a href="/#/add-dwolla-customer-funding-source"><i class="fa fa-circle-o"></i> Bank Accounts</a></li>--}}
{{--                        <!-- <li data-ng-if="user.dwollaCertifyCustomer"><a href="/#/dwolla-pay-vendors"><i class="fa fa-circle-o"></i> Pay Vendor</a></li> -->--}}
{{--                        <li data-ng-if="user.dwollaCertifyCustomer"><a href="/#/dwolla-transfers-log"><i class="fa fa-circle-o"></i> Payment Transfer Logs</a></li>--}}
{{--                        <!-- <li data-ng-if="user.dwollaCertifyCustomer"><a href="/#/delete-dwolla-customer"><i class="fa fa-circle-o"></i> Delete ACH Transfer Customer</a></li>--}}
{{--                        <li data-ng-if="user.dwollaAccessToken && !user.dwollaCustomerId"><a href="/#/dwolla-migrate-customer"><i class="fa fa-circle-o"></i> Migrate Customer</a></li> -->--}}
{{--                    </ul>--}}
{{--                </li>--}}
{{--                <li class="treeview" data-ng-show="user.type == 'agent' && user.newDwolla && user.brokerDwollaCertify">--}}
{{--                    <a href="">--}}
{{--                        <i class="fa fa-money cursor"></i> <span> ACH Transfer</span>--}}
{{--                        <i class="fa fa-angle-left pull-right"></i>--}}
{{--                    </a>--}}
{{--                    <ul class="treeview-menu">--}}
{{--                        <li data-ng-if="!user.dwollaCertifyCustomer"><a href="/#/add-dwolla-receive-only-customer"><i class="fa fa-plus"></i> Create ACH Transfer Account</a></li>--}}
{{--                        <li data-ng-if="user.dwollaCertifyCustomer"><a href="/#/add-dwolla-customer-funding-source"><i class="fa fa-circle-o"></i> Bank Accounts</a></li>--}}
{{--                        <li data-ng-if="user.dwollaCertifyCustomer"><a href="/#/dwolla-transfers-log"><i class="fa fa-circle-o"></i> Payment Transfer Logs</a></li>--}}
{{--                    </ul>--}}
{{--                </li>--}}
{{--                <li class="treeview" data-ng-show="user.type=='broker' && user.newDwolla">--}}
{{--                    <a href="">--}}
{{--                        <i class="fa fa-users"></i>--}}
{{--                        <span>Vendor</span>--}}
{{--                        <i class="fa fa-angle-left pull-right"></i>--}}
{{--                    </a>--}}
{{--                    <ul class="treeview-menu">--}}
{{--                        <li class="active"><a href="/#/vendor-list"><i class="fa fa-circle-o"></i> Vendor</a></li>--}}
{{--                        <li><a href="/#/add-vendor"><i class="fa fa-plus"></i> Add Vendor</a></li>--}}
{{--                    </ul>--}}
{{--                </li>--}}
{{--                <li class="treeview" data-ng-show="user.type=='broker' && user.brokerType==2 && (user.userId==315)">--}}
{{--                    <!-- <li class="treeview"> -->--}}
{{--                    <a href="">--}}
{{--                        <i class="fa fa-user"></i> <span> Admin</span>--}}
{{--                        <i class="fa fa-angle-left pull-right"></i>--}}
{{--                    </a>--}}
{{--                    <ul class="treeview-menu">--}}
{{--                        <li><a href="/#/allow-bulk-upload"><i class="fa fa-upload"></i> Import Agent Roster</a></li>--}}
{{--                        <li><a href="/#/cap-commission-start-point"><i class="fa fa-upload"></i> Import Agent Cap Start Point</a></li>--}}
{{--                        <li><a href="/#/zoho-reports-bulk-sync"><i class="fa fa-upload"></i> Zoho Reports Bulk Sync</a></li>--}}
{{--                        <li><a href="/#/import-transaction"><i class="fa fa-upload"></i> Import Transactions</a></li>--}}
{{--                        <li><a href="/#/import-multi-agent-transaction"><i class="fa fa-upload"></i> Import Multiple Agent Transactions</a></li>--}}
{{--                        <li><a href="/#/import-vendors"><i class="fa fa-circle-o"></i> Import Vendors</a></li>--}}
{{--                        <li><a href="/#/export-transaction"><i class="fa fa-download"></i> Export Transactions</a></li>--}}
{{--                        <li><a href="/#/admin-skyslope"><i class="fa fa-circle-o"></i> SkySlope Raw Data</a></li>--}}
{{--                        <li><a href="/#/test-admin"><i class="fa fa-circle-o"></i> Admin Test</a></li>--}}
{{--                        <li><a href="/#/admin-add-billing-item"><i class="fa fa-circle-o"></i> Admin Billing</a></li>--}}
{{--                        <li><a href="/#/broker-fee"><i class="fa fa-circle-o"></i> Broker Fee</a></li>--}}
{{--                        <li><a href="/#/credit-debit"><i class="fa fa-circle-o"></i> Credit/Debit</a></li>--}}
{{--                        <li><a href="/#/charge-broker-by-date"><i class="fa fa-circle-o"></i> Charge Broker</a></li>--}}
{{--                        <li><a href="/#/extend-trial-period"><i class="fa fa-circle-o"></i> Extend Trial Period</a></li>--}}
{{--                        <li><a href="/#/add-cap"><i class="fa fa-circle-o"></i> Add Transaction Cap</a></li>--}}
{{--                        <li><a href="/#/forgot-password-link"><i class="fa fa-circle-o"></i> Forgot Password Link</a></li>--}}
{{--                        <li><a href="/#/admin-agent-billing-log"><i class="fa fa-circle-o"></i> Agent Billing Log</a></li>--}}
{{--                        <li><a href="/#/admin-agent-onboarding-package"><i class="fa fa-circle-o"></i> Agent Onboarding Package</a></li>--}}
{{--                        <li><a data-ng-click="adminCommissionPlan();"><i class="fa fa-circle-o"></i> Commission Plans</a></li>--}}
{{--                        <li><a href="/#/admin-auto-fill"><i class="fa fa-circle-o"></i> Migration</a></li>--}}
{{--                        <li><a href="/#/admin-user-search"><i class="fa fa-circle-o"></i> User Search</a></li>--}}
{{--                        <li><a href="/#/admin-dwolla-clear-tokens"><i class="fa fa-circle-o"></i> ACH Transfer Clear Tokens</a></li>--}}
{{--                        <li><a href="/#/admin-edit-transactions"><i class="fa fa-circle-o"></i> Edit Transactions</a></li>--}}
{{--                        <li><a href="/#/admin-sync-transaction"><i class="fa fa-circle-o"></i> Sync Transactions For Report</a></li>--}}
{{--                        <!-- <li><a href="/#/admin-company-report"><i class="fa fa-circle-o"></i> Company Report</a></li> -->--}}
{{--                        <li><a href="/#/admin-reports"><i class="fa fa-circle-o"></i> Reports</a></li>--}}
{{--                        <li><a href="/#/admin-add-report-data"><i class="fa fa-circle-o"></i>Add Report Data</a></li>--}}
{{--                        <li><a href="/#/docusign-log"><i class="fa fa-circle-o"></i> Onboarding Templates</a></li>--}}
{{--                        <li><a href="/#/admin-reset-onboard"><i class="fa fa-circle-o"></i> Restore Agent template</a></li>--}}
{{--                        <li><a href="/#/admin-update-users-email"><i class="fa fa-circle-o"></i> Update Users Email</a></li>--}}
{{--                        <li><a href="/#/admin-undelete-agent"><i class="fa fa-circle-o"></i> Undelete Agent</a></li>--}}
{{--                        <li><a href="/#/admin-mls-db"><i class="fa fa-circle-o"></i> Add Broker MLS</a></li>--}}
{{--                        <li><a href="/#/admin-agent-activity-log"><i class="fa fa-circle-o"></i> Agent Activity Log</a></li>--}}
{{--                        <li><a href="/#/admin-undelete-transaction"><i class="fa fa-circle-o"></i> Undelete Transaction</a></li>--}}
{{--                        <li><a href="/#/admin-add-start-point"><i class="fa fa-circle-o"></i> Add Start Point</a></li>--}}
{{--                        <li><a href="/#/admin-add-company"><i class="fa fa-circle-o"></i> Add Company</a></li>--}}
{{--                        <li><a href="/#/dwolla-activity-log"><i class="fa fa-circle-o"></i> ACH Transfer Activity Log</a></li>--}}
{{--                        <li><a href="/#/dwolla-log"><i class="fa fa-circle-o"></i> ACH Transfer Response Log</a></li>--}}
{{--                        <li><a href="/#/dwolla-webhook-log"><i class="fa fa-circle-o"></i> ACH Transfer Webhook Log</a></li>--}}
{{--                        <li><a href="/#/admin-new-dwolla-functionality"><i class="fa fa-circle-o"></i> New Dwolla Functionality</a></li>--}}
{{--                        <li><a href="/#/admin-dwolla-webhook-list"><i class="fa fa-circle-o"></i> ACH Transfer Webhook List</a></li>--}}
{{--                        <li><a href="/#/admin-dwolla-save-funding-source-log-list"><i class="fa fa-circle-o"></i> Save Funding Source In Our DB</a></li>--}}
{{--                        <li><a href="/#/admin-delete-other-income"><i class="fa fa-circle-o"></i> Delete Other Income</a></li>--}}
{{--                        <li><a href="/#/admin-delete-agent-credit-entry"><i class="fa fa-circle-o"></i> Delete Agent Credit Entry</a></li>--}}
{{--                        <li><a href="/#/admin-mls-transactions-view"><i class="fa fa-circle-o"></i> MLS Search</a></li>--}}
{{--                        <li><a href="/#/admin-auto-switch-commission-plan"><i class="fa fa-circle-o"></i> Auto Switch Commission Plan</a></li>--}}
{{--                        <li><a href="/#/admin-edit-account-info"><i class="fa fa-circle-o"></i> Edit Account Info</a></li>--}}
{{--                        <li><a href="/#/admin-update-commission-plan"><i class="fa fa-circle-o"></i> Update Agent Commission Plan</a></li>--}}
{{--                        <li><a href="/#/admin-turn-on-feature"><i class="fa fa-circle-o"></i> Agent Billing Import</a></li>--}}
{{--                        <li><a href="/#/admin-activate-agents"><i class="fa fa-circle-o"></i> Activate Agent's</a></li>--}}
{{--                        <li><a href="/#/admin-add-license-types"><i class="fa fa-circle-o"></i> Add License Types</a></li>--}}
{{--                        <li><a href="/#/admin-import-co-broke"><i class="fa fa-circle-o"></i> Co-Broke Import</a></li>--}}
{{--                        <li><a href="/#/admin-copy-agents"><i class="fa fa-circle-o"></i> Move Agents</a></li>--}}
{{--                        <li><a href="/#/admin-certify-beneficial-owner"><i class="fa fa-circle-o"></i> Certify Beneficial Ownership</a></li>--}}
{{--                        <li><a href="/#/remove-two-factor"><i class="fa fa-circle-o"></i> Remove 2FA</a></li>--}}
{{--                        <li><a href="/#/admin-agent-credit-card-expiry-email"><i class="fa fa-circle-o"></i> Agent Credit Card Expiry Email</a></li>--}}
{{--                        <li><a href="/#/admin-field-validation"><i class="fa fa-circle-o"></i> Admin Field Validation</a></li>--}}
{{--                        <li><a href="/#/admin-import-agent-data"><i class="fa fa-circle-o"></i> Import Agent Data</a></li>--}}
{{--                        <li><a href="/#/save-update-next-qb-txn-no"><i class="fa fa-circle-o"></i> Add Next Qb Txn No.</a></li>--}}
{{--                        <li><a href="/#/import-title-company"><i class="fa fa-circle-o"></i> Import Title Companies</a></li>--}}
{{--                        <li><a href="/#/admin-companies-import"><i class="fa fa-upload"></i>Import Companies</a></li>--}}
{{--                        <li><a href="/#/admin-weichert"><i class="fa fa-upload"></i>Admin Weichert</a></li>--}}
{{--                        <li><a href="/#/migrate-company-data"><i class="fa fa-circle-o"></i>Migrate Company Data</a></li>--}}
{{--                        <li><a href="/#/admin-estimated-delivery-date-updation"><i class="fa fa-circle-o"></i> Estimated Delivery Date Updation</a></li>--}}
{{--                        <li><a href="/#/admin-agent-signed-document"><i class="fa fa-circle-o"></i> Agent Signed Document</a></li>--}}
{{--                        <li><a href="/#/admin-realogy"><i class="fa fa-circle-o"></i>Realogy</a></li>--}}
{{--                    </ul>--}}
{{--                </li>--}}

{{--                <li class="treeview" data-ng-show="user.type=='broker' && user.brokerType==2 && user.userId==4301901">--}}
{{--                    <a href="">--}}
{{--                        <i class="fa fa-user"></i> <span> Admin</span>--}}
{{--                        <i class="fa fa-angle-left pull-right"></i>--}}
{{--                    </a>--}}
{{--                    <ul class="treeview-menu">--}}
{{--                        <!-- BROK-2843 -->--}}
{{--                        <!-- <li><a href="/#/charge-broker"><i class="fa fa-credit-card" aria-hidden="true"></i> Charge Broker</a></li>--}}
{{--                        <li><a href="/#/test-admin1"><i class="fa fa-circle-o"></i> Admin Test</a></li> -->--}}
{{--                        <li><a href="/#/agent-billing-status"><i class="fa fa-circle-o"></i> Admin Billing Status</a></li>--}}
{{--                    </ul>--}}
{{--                </li>--}}

{{--                <li class="treeview" data-ng-show="user.type=='agent'">--}}
{{--                    <a href="">--}}
{{--                        <i class="fa fa-user"></i> <span>My Account </span>--}}
{{--                        <i class="fa fa-angle-left pull-right"></i>--}}
{{--                    </a>--}}
{{--                    <ul class="treeview-menu">--}}
{{--                        <li><a href="/#/agent-account"><i class="fa fa-circle-o"></i> Account Settings</a></li>--}}
{{--                        <li><a href="/#/agent-billing-log"><i class="fa fa-circle-o"></i> Billing Log</a></li>--}}
{{--                        <li><a href="/#/agent-commission-plan" data-ng-show="user.isAllowAgentToViewCommissionPlan || user.isAgentAllowToViewCommissionPlan"><i class="fa fa-circle-o"></i> Commission Plan</a></li>--}}
{{--                        <li><a href="/#/agent-documents"><i class="fa fa-circle-o"></i> Documents</a></li>--}}
{{--                        <li><a href="/#/agent-invoice-billing" data-ng-show="user.companyId == 314 || user.companyId == 5"><i class="fa fa-circle-o"></i> My Invoice</a></li>--}}
{{--                        <li><a href="/#/other-income"><i class="fa fa-circle-o"></i> Other Income</a></li>--}}
{{--                        <li><a href="/#/payment-log"><i class="fa fa-circle-o"></i> Payment Log</a></li>--}}
{{--                        <li><a href="/#/agent-portal-checklist" data-ng-show="user.isShowChecklistInAgentPortal"><i class="fa fa-circle-o"></i> Task Checklist</a></li>--}}
{{--                        <!-- <li data-ng-if="user.gatewayId"><a href="/#/credit-card-info"><i class="fa fa-circle-o"></i> Credit Card Info</a></li> -->--}}
{{--                    </ul>--}}
{{--                </li>--}}
{{--                <li class="treeview" data-ng-show="user.type=='agent'">--}}
{{--                    <a href="">--}}
{{--                        <i class="fa fa-cog"></i> <span>Settings</span>--}}
{{--                        <i class="fa fa-angle-left pull-right"></i>--}}
{{--                    </a>--}}
{{--                    <ul class="treeview-menu">--}}
{{--                        <li data-ng-show="user.show1099Settings"><a href="/#/agent-1099-settings"><i class="fa fa-circle-o"></i> 1099 Settings</a></li>--}}
{{--                        <li data-ng-show="user.isPlaidBroker"><a href="/#/bank-accounts"><i class="fa fa-circle-o"></i> Bank Accounts</a></li>--}}
{{--                        <li><a href="" data-ng-click="integrationUrl();" data-ng-if="user.isDotloop"><i class="fa fa-circle-o"></i> Integrations</a></li> <!-- data-ng-if="user.userId==344848" -->--}}
{{--                        <li><a href="/#/manage-tags"><i class="fa fa-circle-o"></i> Tags</a></li>--}}
{{--                        <!--<li data-ng-if="user.isPlaidBroker && !user.newDwolla"><a href="/#/direct-deposit"><i class="fa fa-circle-o"></i> Payments</a></li>-->--}}
{{--                        <li class="treeview">--}}
{{--                            <a href="" data-ng-click="viewVendors()">--}}
{{--                                <i class="fa fa-circle-o"></i> <span>Vendors</span>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                        <li><a href="/#/broker-permission"><i class="fa fa-key"></i> Permissions</a></li>--}}
{{--                    </ul>--}}
{{--                </li>--}}

{{--                <li data-ng-if="user.agentKvCoreId > 0 && user.isKvcoreAgentIntegrationEnabled">--}}
{{--                    <a href="" data-ng-click="kvCoreSSO();">--}}
{{--                        <img src="pages/img/kvcore-icon.png" height="20"/> kvCore--}}
{{--                    </a>--}}
{{--                </li>--}}

{{--                <li data-ng-if="!(user.companyId === 463645 && user.type === 'agent')">--}}
{{--                    <a href="">--}}
{{--                        <i class="fa fa-question"></i> <span>Help</span>--}}
{{--                        <i class="fa fa-angle-left pull-right"></i>--}}
{{--                    </a>--}}
{{--                    <ul class="treeview-menu">--}}
{{--                        <!-- <li><a href="https://www.youtube.com/c/brokersumorealestate" target="_blank"><i class="fa fa-video-camera"></i> Videos</a></li> -->--}}
{{--                        <li><a href="/cdn-cgi/l/email-protection#7c1e0e1317190e0f0911133c15120f1518190e191d10190f081d0819521f1311" target="_blank"><i class="fa  fa-envelope-o"></i> Email Support</a></li>--}}
{{--                        <li><a href="https://help.insiderealestate.com/en/collections/1898269-core-back-office" target="_blank"><i class="fa fa-lightbulb-o"></i> Support Center</a></li>--}}
{{--                        <li><a href="https://insiderealestate.thinkific.com/api/sso/v2/sso/jwt?jwt={{ user.thinkificToken }}&return_to=https%3A%2F%2Flearn.insiderealestate.com/" target="_blank"><img height="15" src="pages/img/learning-bs.png" /> Learning Portal</a></li>--}}
{{--                    </ul>--}}
{{--                </li>--}}
{{--            </ul>--}}
{{--        </section>--}}
{{--        <!-- /.sidebar -->--}}
{{--    </aside>--}}
{{--    <!-- Content Wrapper. Contains page content -->--}}


{{--    <!-- for staff user with permissions -->--}}
{{--    <aside class="main-sidebar" data-ng-show="authenticate==true && user.type=='broker' && user.brokerType == 3 && !user.superAdmin && (user.agentAccessOnly || user.transactionAccessOnly || user.reportAccessOnly)" data-ng-if="ShowHeader">--}}
{{--        <section class="sidebar">--}}
{{--            <ul class="sidebar-menu">--}}
{{--                <li class="treeview" data-ng-show="user.agentAccessOnly">--}}
{{--                    <a href="">--}}
{{--                        <i class="fa fa-users"></i>--}}
{{--                        <span>Agents</span>--}}
{{--                        <i class="fa fa-angle-left pull-right"></i>--}}
{{--                    </a>--}}
{{--                    <ul class="treeview-menu">--}}
{{--                        <li class="active"><a href="" data-ng-click="viewAgent();"><i class="fa fa-circle-o"></i>Agents ({{user.agentCount}})</a></li>--}}
{{--                        <li><a href="" data-ng-click="addAgent();"><i class="fa fa-circle-o"></i>+ Add Agents</a></li>--}}
{{--                        <li><a href="/#/manage-agent-office-license"><i class="fa fa-circle-o"></i>+ Add Agents Office/License</a></li>--}}
{{--                        <li><a href="/#/agent-billing"><i class="fa fa-circle-o"></i>Agent Billing</a></li>--}}
{{--                        <li><a href="/#/other-income-log"><i class="fa fa-circle-o"></i>Other Income Log</a></li>--}}
{{--                        <li class="treeview">--}}
{{--                            <a href="/#/office-documents">--}}
{{--                                <i class="fa fa-file" aria-hidden="true"></i>--}}
{{--                                <span> Office Documents</span>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                        <li>--}}
{{--                            <a href="" data-ng-if="user.isProAccount" data-ng-click="teams();">--}}
{{--                                <i class="fa fa-circle-o"></i>Teams--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                    </ul>--}}
{{--                </li>--}}
{{--                <li class="treeview" data-ng-show="user.transactionAccessOnly">--}}
{{--                    <a href="">--}}
{{--                        <i class="fa fa-exchange"></i><span>Transactions</span>--}}
{{--                        <i class="fa fa-angle-left pull-right"></i>--}}
{{--                    </a>--}}
{{--                    <ul class="treeview-menu">--}}
{{--                        <li><a href="" data-ng-click="viewTransaction();"><i class="fa fa-circle-o"></i> View Transactions</a></li>--}}
{{--                        <li><a href="/#/add-transaction"><i class="fa fa-plus"></i> Add Transaction</a></li>--}}
{{--                        <li><a data-ng-click="addDemand();"><i class="fa fa-plus"></i> Create Disbursement</a></li>--}}
{{--                        <li><a href="/#/pre-demand"><i class="fa fa-circle-o"></i> Pre Disbursement</a></li>--}}
{{--                        <li><a href="/#/trust-account-deposits"><i class="fa fa-circle-o"></i> Trust Account / Deposits</a></li>--}}
{{--                        <li data-ng-show="(user.type=='broker' && (user.dotLoopSetUp || user.isCompanyDotloopSetUp))"><a href="" data-ng-click="dotloopSyncUrl();"><i class="fa fa-circle-o"></i> Dotloop</a></li>--}}
{{--                        <li data-ng-show="user.skySlopeSetup"><a href="/#/sky-slope-integration-broker"><i class="fa fa-circle-o"></i> SkySlope</a></li>--}}
{{--                        <li data-ng-show="user.isProAccount"><a href="/#/mls-integration"><i class="fa fa-circle-o"></i> MLS</a></li>--}}
{{--                        <li data-ng-show="user.isDocusignTransaction"><a href="/#/docusign-transaction-log"><i class="fa fa-circle-o"></i> Docusign Transaction Log</a></li><!--(user.companyId == 5 || user.companyId == 314 || user.companyId == 3841048 || user.companyId == 1048405 || user.companyId == 3360014 || user.companyId == 4880347)-->--}}
{{--                    </ul>--}}
{{--                </li>--}}
{{--                <li class="treeview" data-ng-show="user.reportAccessOnly">--}}
{{--                    <a href="/#/reports">--}}
{{--                        <i class="fa fa-pie-chart"></i><span> Reports</span>--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--                <li class="treeview">--}}
{{--                    <a href="">--}}
{{--                        <i class="fa fa-cog"></i> <span>Settings</span>--}}
{{--                        <i class="fa fa-angle-left pull-right"></i>--}}
{{--                    </a>--}}
{{--                    <ul class="treeview-menu">--}}
{{--                        <li><a href="/#/staff-user-account-info"><i class="fa fa-key"></i> Reset Password</a></li>--}}
{{--                    </ul>--}}
{{--                </li>--}}
{{--                <li>--}}
{{--                    <a href="">--}}
{{--                        <i class="fa fa-question"></i> <span>Help</span>--}}
{{--                        <i class="fa fa-angle-left pull-right"></i>--}}
{{--                    </a>--}}
{{--                    <ul class="treeview-menu">--}}
{{--                        <!-- <li><a href="https://www.youtube.com/c/brokersumorealestate" target="_blank"><i class="fa fa-video-camera"></i> Videos</a></li> -->--}}
{{--                        <li><a href="/cdn-cgi/l/email-protection#6f0d1d00040a1d1c1a02002f06011c060b0a1d0a0e030a1c1b0e1b0a410c0002" target="_blank"><i class="fa  fa-envelope-o"></i> Email Support</a></li>--}}
{{--                        <li><a href="https://help.insiderealestate.com/en/collections/1898269-core-back-office" target="_blank"><i class="fa fa-lightbulb-o"></i> Support Center</a></li>--}}
{{--                        <li><a href="https://insiderealestate.thinkific.com/api/sso/v2/sso/jwt?jwt={{ user.thinkificToken }}&return_to=https%3A%2F%2Flearn.insiderealestate.com/" target="_blank"><img height="15" src="pages/img/learning-bs.png" /> Learning Portal</a></li>--}}
{{--                    </ul>--}}
{{--                </li>--}}
{{--            </ul>--}}
{{--        </section>--}}
{{--        <!-- /.sidebar -->--}}
{{--    </aside>--}}

{{--    <aside class="main-sidebar" data-ng-show="((authenticate==true && user.type=='vendor') || (user.loggedIn=='2' && !user.accessAgentPortal))" data-ng-if="ShowHeader">--}}
{{--        <!-- <aside class="main-sidebar" data-ng-show="(authenticate==true && user.type=='vendor')" data-ng-if="ShowHeader"> -->--}}
{{--        <section class="sidebar">--}}
{{--            <ul class="sidebar-menu">--}}
{{--                <li class="treeview" data-ng-if="!user.dwollaCertifyCustomer && user.brokerDwollaCertify">--}}
{{--                    <a href="/#/add-dwolla-receive-only-customer"><i class="fa fa-plus"></i> Create ACH Transfer Account</a>--}}
{{--                </li>--}}
{{--                <li class="treeview" data-ng-if="user.dwollaCertifyCustomer && user.brokerDwollaCertify">--}}
{{--                    <a href="/#/add-dwolla-customer-funding-source"><i class="fa fa-circle-o"></i> Bank Accounts</a>--}}
{{--                </li>--}}
{{--                <li class="treeview" data-ng-if="user.dwollaCertifyCustomer && user.brokerDwollaCertify">--}}
{{--                    <a href="/#/dwolla-transfers-log"><i class="fa fa-circle-o"></i> Payment Transfer Logs</a>--}}
{{--                </li>--}}
{{--                <li class="treeview" data-ng-if="user.type=='vendor'">--}}
{{--                    <a href="/#/vendor-account-info"><i class="fa fa-user"></i> Account Info</a>--}}
{{--                </li>--}}
{{--                <!-- <li class="treeview" data-ng-if="user.dwollaCertifyCustomer && user.loggedIn=='2'">--}}
{{--                    <a href="/#/delete-dwolla-customer"><i class="fa fa-circle-o"></i> Delete ACH Transfer Customer</a>--}}
{{--                </li>--}}
{{--                <li class="treeview" data-ng-if="user.loggedIn=='2' && user.dwollaAccessToken && !user.dwollaCustomerId">--}}
{{--                 <a href="/#/dwolla-migrate-customer"><i class="fa fa-circle-o"></i> Migrate Customer</a>--}}
{{--                </li> -->--}}
{{--                <li>--}}
{{--                    <a href="">--}}
{{--                        <i class="fa fa-question"></i> <span>Help</span>--}}
{{--                        <i class="fa fa-angle-left pull-right"></i>--}}
{{--                    </a>--}}
{{--                    <ul class="treeview-menu">--}}
{{--                        <!-- <li><a href="https://www.youtube.com/c/brokersumorealestate" target="_blank"><i class="fa fa-video-camera"></i> Videos</a></li> -->--}}
{{--                        <li><a href="/cdn-cgi/l/email-protection#ddbfafb2b6b8afaea8b0b29db4b3aeb4b9b8afb8bcb1b8aea9bca9b8f3beb2b0" target="_blank"><i class="fa  fa-envelope-o"></i> Email Support</a></li>--}}
{{--                        <li><a href="https://help.insiderealestate.com/en/collections/1898269-core-back-office" target="_blank"><i class="fa fa-lightbulb-o"></i> Support Center</a></li>--}}
{{--                        <li><a href="https://insiderealestate.thinkific.com/api/sso/v2/sso/jwt?jwt={{ user.thinkificToken }}&return_to=https%3A%2F%2Flearn.insiderealestate.com/" target="_blank"><img height="15" src="pages/img/learning-bs.png" /> Learning Portal</a></li>--}}
{{--                    </ul>--}}
{{--                </li>--}}
{{--            </ul>--}}
{{--        </section>--}}
{{--        <!-- /.sidebar -->--}}
{{--    </aside>--}}

{{--    <!-- Content Wrapper. Contains page content -->--}}

{{--    <div data-ng-view data-ng-class="authenticate ? (user.type=='vendor' || user.type=='broker'|| user.loggedIn=='2' || user.isPlaidAgent? 'content-wrapper account_info' : 'content-wrapper onboarding_page') : 'register-box'" data-ng-show="authenticate">--}}
{{--    </div>--}}
{{--    <!-- /.content-wrapper -->--}}
{{--    <footer class="main-footer" data-ng-class="(user.type=='agent' && user.loggedIn!='2' && !user.isPlaidAgent) ? 'margin-left-0' : ''" data-ng-if="authenticate">--}}
{{--        <strong data-ng-if="!user.isBranding">Copyright &copy; 2021 <a href="#">Broker Sumo</a>. </strong><span data-ng-if="!user.isBranding">All rights reserved.</span>--}}
{{--    </footer>--}}

{{--    <div class="clearfix"></div>--}}
{{--</div><!-- ./wrapper -->--}}

{{--<script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script type="text/javascript">--}}
{{--    var version = "2021-05-19";--}}
{{--    function loadScriptFile(iSrc) {--}}
{{--        var sc = "<script type="+"'text/javascript'" + "src='https://brokersumo.com/"+iSrc+"?ver="+version+"'" + ">"+"</"+"script>";--}}
{{--        document.write(sc);--}}
{{--    }--}}
{{--</script>--}}
{{--<script src="https://js.stripe.com/v2/" type="text/javascript"></script>--}}
{{--<script src="https://brokersumo.com/js/jquery.moment.js" type="text/javascript"></script>--}}
{{--<!-- <script src="js/jquery.js" type="text/javascript" ></script> -->--}}
{{--<script src="https://brokersumo.com/js/jquery.ui.js" type="text/javascript"></script>--}}
{{--<script src="https://brokersumo.com/js/jquery.icheck.js" type="text/javascript"></script>--}}
{{--<script src="https://brokersumo.com/js/jquery.fancytree.js" type="text/javascript"></script>--}}
{{--<script src="https://brokersumo.com/js/jquery.validator.js" type="text/javascript"></script>--}}
{{--<script src="https://brokersumo.com/js/jquery.mask.js" type="text/javascript"></script>--}}
{{--<script type="text/javascript">loadScriptFile("js/jquery.chosen.js")</script>--}}
{{--<script src="https://brokersumo.com/js/jquery.qtip.js" type="text/javascript"></script>--}}
{{--<!-- <script src="js/jquery.daterangepicker.js" type="text/javascript"></script> -->--}}
{{--<script src="https://brokersumo.com/js/daterangepicker.js" type="text/javascript"></script>--}}
{{--<script src="//blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js"></script>--}}
{{--<script src="https://brokersumo.com/js/jquery.fileupload.js" type="text/javascript"></script>--}}
{{--<script src="https://brokersumo.com/js/jquery.fileupload-process.js" type="text/javascript"></script>--}}
{{--<script src="https://brokersumo.com/js/jquery.fileupload-image.js" type="text/javascript"></script>--}}
{{--<script src="https://brokersumo.com/js/jquery.fileupload-validate.js" type="text/javascript"></script>--}}

{{--<script src="js/bootstrap-switch.js" type="text/javascript"></script>--}}

{{--<script src="https://brokersumo.com/js/jquery.bootstrap.js" type="text/javascript"></script>--}}
{{--<script src="https://brokersumo.com/js/jquery.app.js" type="text/javascript"></script>--}}
{{--<script src="https://brokersumo.com/js/jquery.datatables.js" type="text/javascript"></script>--}}
{{--<script src="https://brokersumo.com/js/external/bootstrap/bootstrap-tagsinput.js" type="text/javascript"></script>--}}
{{--<!-- <script src="https://brokersumo.com/js/external/angularhttps://brokersumo.com/js/angular.min.js" type="text/javascript"></script>--}}
{{--<script src="https://brokersumo.com/js/external/angularjs/angular-resource.js" type="text/javascript"></script>--}}
{{--<script src="js/external/angularjs/angular-route.js" type="text/javascript"></script> -->--}}
{{--<script src="https://brokersumo.com/js/external/angularjs/angular-chosen.js" type="text/javascript"></script>--}}
{{--<script src="https://brokersumo.com/js/external/angular-file-upload/angular-file-upload.js" type="text/javascript"></script>--}}
{{--<script type="text/javascript">loadScriptFile("js/external/satellizer/satellizer.js")</script>--}}
{{--<script src="https://brokersumo.com/js/external/moment/moment.min.js" type="text/javascript"></script>--}}
{{--<script src="https://brokersumo.com/js/external/ng-storage/ngStorage.js" type="text/javascript"></script>--}}
{{--<script type="text/javascript">--}}
{{--    loadScriptFile("js/external/autocomplete/autocomplete.js");--}}
{{--    loadScriptFile("js/custom/common/common.js");--}}
{{--</script>--}}
{{--<script src="https://brokersumo.com/js/external/angularjs/angular-sanitize.js" type="text/javascript"></script>--}}
{{--<!-- <script src="https://cdn.plaid.com/link/stable/link-initialize.js" type="text/javascript"></script> -->--}}
{{--<script src="https://cdn.plaid.com/link/v2/stable/link-initialize.js" type="text/javascript"></script>--}}
{{--<script src="https://brokersumo.com/js/angular-bootstrap-switch.min.js" type="text/javascript"></script>--}}
{{--<script type="text/javascript" src="//s3.amazonaws.com/cdn.hellofax.com/js/embedded.js"></script>--}}
{{--<script src="https://brokersumo.com/js/external/angularjs/Chart.js" type="text/javascript"></script>--}}
{{--<script src="https://brokersumo.com/js/external/angularjs/angular-chart.min.js" type="text/javascript"></script>--}}
{{--<script src="https://brokersumo.com/js/external/angular-mask/mask.min.js" type="text/javascript"></script>--}}
{{--<script src="https://brokersumo.com/js/external/currency-format/format-as-currency.js" type="text/javascript"></script>--}}
{{--<script src="https://brokersumo.com/js/external/ui-boostrap/ui-bootstrap-tpls-2.5.0.js" type="text/javascript"></script>--}}
{{--<script src="https://brokersumo.com/js/external/print-js/print.min.js" type="text/javascript"></script>--}}

{{--<!-- full Calendar 3.4.0 -->--}}
{{--<script src="https://brokersumo.com/js/external/fullcalendar-3.4.0/fullcalendar.min.js" type="text/javascript"></script>--}}

{{--<!-- custom angular js files starts from here -->--}}

{{--<!-- Services -->--}}
{{--<script type="text/javascript">--}}
{{--    loadScriptFile("js/custom/angularjs/app/brokersumo-app.js");--}}
{{--    loadScriptFile("js/custom/angularjs/constants/brokersumo-constants.js");--}}
{{--    loadScriptFile("js/custom/angularjs/common-function/common-functions-service.js");--}}
{{--    loadScriptFile("js/custom/angularjs/broker-account-info/service/account-info-service.js");--}}
{{--    loadScriptFile("js/custom/angularjs/add-agent/service/agent-service.js");--}}
{{--    loadScriptFile("js/custom/angularjs/agent-onboarding/service/agent-onboarding-service.js");--}}
{{--    loadScriptFile("js/custom/angularjs/broker-signup/service/broker-register-service.js");--}}
{{--    loadScriptFile("js/custom/angularjs/agent-account/service/agent-account-service.js");--}}
{{--    loadScriptFile("js/custom/angularjs/agent-document-list/service/agent-document-list-service.js");--}}
{{--    loadScriptFile("js/custom/angularjs/agent-commission/service/agent-commission-service.js");--}}
{{--    loadScriptFile("js/custom/angularjs/agent-billing-log/service/agent-billing-log-service.js");--}}
{{--    loadScriptFile("js/custom/angularjs/commission-plan/service/commission-plan-service.js");--}}
{{--    loadScriptFile("js/custom/angularjs/office-location/service/office-location-service.js");--}}
{{--    loadScriptFile("js/custom/angularjs/transactions/service/transaction-service.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-transactions/service/new-transaction-service.js");--}}
{{--    loadScriptFile("js/custom/angularjs/dashboard/service/dashboard-service.js");--}}
{{--    loadScriptFile("js/custom/angularjs/user-dashboard/service/dashboard-service.js");--}}
{{--    loadScriptFile("js/custom/angularjs/credit-card-setting/service/credit-card-setting-service.js");--}}
{{--    loadScriptFile("js/custom/angularjs/integrations/service/integrations-service.js");--}}
{{--    loadScriptFile("js/custom/angularjs/allow-bulk-upload/service/allow-bulk-upload-service.js");--}}
{{--    loadScriptFile("js/custom/angularjs/test-admin/service/test-admin-service.js");--}}
{{--    loadScriptFile("js/custom/angularjs/office-documents/service/office-documents-service.js");--}}
{{--    loadScriptFile("js/custom/angularjs/charge-broker/service/charge-broker-service.js");--}}
{{--    loadScriptFile("js/custom/angularjs/credit-debit/service/credit-debit-service.js");--}}
{{--    loadScriptFile("js/custom/angularjs/extend-trial-period/service/extend-trial-period-service.js");--}}
{{--    loadScriptFile("js/custom/angularjs/agent-permission/service/agent-permission-service.js");--}}
{{--    loadScriptFile("js/custom/angularjs/commission-plan-breakdown/service/commission-plan-breakdown-service.js");--}}
{{--    loadScriptFile("js/custom/angularjs/direct-deposit/service/direct-deposit-service.js");--}}
{{--    loadScriptFile("js/custom/angularjs/pay-agent/service/pay-agent-service.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-pay-agent/service/new-pay-agent-service.js");--}}
{{--    loadScriptFile("js/custom/angularjs/vendors/service/vendors-service.js");--}}
{{--    loadScriptFile("js/custom/angularjs/agent-1099s/service/agent-1099s-service.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-reports/service/new-reports-service.js");--}}
{{--    loadScriptFile("js/custom/angularjs/staff-user/service/staff-user-service.js");--}}
{{--    loadScriptFile("js/custom/angularjs/admin-add-billing-item/service/admin-add-billing-item-service.js");--}}
{{--    loadScriptFile("js/custom/angularjs/payment-log/service/payment-log-service.js");--}}
{{--    loadScriptFile("js/custom/angularjs/credit-card-info/service/credit-card-info-service.js");--}}
{{--    loadScriptFile("js/custom/angularjs/import-transaction/service/import-transaction-service.js");--}}
{{--    loadScriptFile("js/custom/angularjs/forgot-password-link/service/forgot-password-link-service.js");--}}
{{--    loadScriptFile("js/custom/angularjs/admin-agent-billing-log/service/admin-agent-billing-log-service.js");--}}
{{--    loadScriptFile("js/custom/angularjs/admin-agent-onboarding-package/service/admin-agent-onboarding-package-service.js");--}}
{{--    loadScriptFile("js/custom/angularjs/teams/service/teams-service.js");--}}
{{--    loadScriptFile("js/custom/angularjs/auto-fill-items/service/auto-fill-service.js");--}}
{{--    loadScriptFile("js/custom/angularjs/admin-mls-db/service/admin-mls-db-service.js");--}}
{{--    loadScriptFile("js/custom/angularjs/dwolla/service/dwolla-service.js");--}}
{{--    loadScriptFile("js/custom/angularjs/admin-reports/service/admin-reports-service.js");--}}
{{--    loadScriptFile("js/custom/angularjs/agent-commission-plan/service/agent-commission-plan-service.js");--}}
{{--    loadScriptFile("js/custom/angularjs/import-billing/service/import-billing-service.js");--}}
{{--    loadScriptFile("js/custom/angularjs/remax-integration/service/remax-integration-service.js");--}}
{{--    loadScriptFile("js/custom/angularjs/view-agent/service/agent-view-service.js");--}}
{{--    loadScriptFile("js/custom/angularjs/view-agent/service/agent-invoice-service.js");--}}
{{--    loadScriptFile("js/custom/angularjs/view-agent/service/agent-history-service.js");--}}
{{--    loadScriptFile("js/custom/angularjs/organization/service/organization-service.js");--}}
{{--    loadScriptFile("js/custom/angularjs/authy/service/authy-service.js");--}}
{{--    loadScriptFile("js/custom/angularjs/agent-billing-status/service/agent-billing-status-service.js");--}}
{{--    loadScriptFile("js/custom/angularjs/city-by-zipcode-service/city-by-zipcode-service.js");--}}
{{--    loadScriptFile("js/custom/angularjs/edit-title-attorneys/service/auto-fill-title-attorneys-service.js");--}}
{{--    loadScriptFile("js/custom/angularjs/edit-title-attorneys/service/title-company-service.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-transactions/service/include-exclude-fields-service.js");--}}
{{--    loadScriptFile("js/custom/angularjs/realogy-integration/service/realogy-integration-service.js");--}}
{{--    loadScriptFile("js/custom/angularjs/agent-view/service/agent-view-service.js");--}}
{{--    loadScriptFile("js/custom/angularjs/admin-agent-credit-card-expiry-email/service/admin-agent-credit-card-expiry-email-service.js");--}}
{{--    loadScriptFile("js/custom/angularjs/bank-deposit/service/bank-deposit-service.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-transactions/service/field-validation-service.js");--}}
{{--    loadScriptFile("js/custom/angularjs/commission-plan/service/commission-plan-activity-service.js");--}}
{{--    loadScriptFile("js/custom/angularjs/agci-levels/service/agci-level-service.js?");--}}
{{--    loadScriptFile("js/custom/angularjs/admin-import-agent-data/service/admin-import-agent-data-service.js");--}}
{{--    loadScriptFile("js/custom/angularjs/view-agent/service/agent-billing-activity-service.js");--}}
{{--    loadScriptFile("js/custom/angularjs/import-agent-first-team-custom-fields/service/import-agent-first-team-custom-fields-service.js");--}}
{{--    loadScriptFile("js/custom/angularjs/import-vendors/service/import-vendors-service.js");--}}
{{--    loadScriptFile("js/custom/angularjs/import-attorneys/service/import-attorneys-service.js");--}}
{{--    loadScriptFile("js/custom/angularjs/transaction-master-data/controller/transaction-master-data-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/agent-billings-setup/service/agent-billings-setup-service.js");--}}
{{--</script>--}}

{{--<!-- Controllers -->--}}
{{--<script type="text/javascript">--}}
{{--    loadScriptFile("js/custom/angularjs/broker-signup/controller/broker-register-controller.js")--}}
{{--    loadScriptFile("js/custom/angularjs/broker-template/controller/broker-template-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/broker-account-info/controller/account-info-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/agent-onboarding/controller/agent-onboarding-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/login/controller/login-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/add-agent/controller/add-agent-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/agent-account/controller/agent-account-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/agent-commission/controller/agent-commission-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/agent-billing-log/controller/agent-billing-log-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/agent-document-list/controller/agent-document-list-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/view-agent/controller/view-agent-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/commission-plan/controller/commission-plan-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/commission-plan/controller/copy-commission-plan-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/commission-plan/controller/admin-commission-plan-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/commission-plan/controller/copy-admin-commission-plan-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-transactions/controller/new-add-transaction-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-transactions/controller/new-view-transactions-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-transactions/controller/new-add-demand-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-transactions/controller/add-demand-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/trust-account-deposits/service/trust-account-deposits-service.js");--}}
{{--    loadScriptFile("js/custom/angularjs/trust-account-deposits/controller/trust-account-deposits-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/setup-checklist/controller/setup-checklist-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/staff-user/controller/staff-users-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/dashboard/controller/dashboard-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/user-dashboard/controller/user-dashboard-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/credit-card-setting/controller/credit-card-setting-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/integrations/controller/integrations-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/integrations/controller/integrations-docusign-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/allow-bulk-upload/controller/allow-bulk-upload-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/test-admin/controller/test-admin-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/office-documents/controller/office-documents-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/charge-broker/controller/charge-broker-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/credit-debit/controller/credit-debit-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/extend-trial-period/controller/extend-trial-period-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/agent-permission/controller/agent-permission-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/commission-plan-breakdown/controller/commission-plan-breakdown-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/direct-deposit/controller/direct-deposit-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-pay-agent/controller/new-pay-agent-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/agent-1099s/controller/agent-1099s-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/agent-1099s/controller/tax1099-integration-controller-broker.js");--}}
{{--    loadScriptFile("js/custom/angularjs/agent-1099s/controller/tax1099-agent-integration-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/reports/controller/reports-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-reports/controller/new-reconciliation-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-reports/controller/new-office-commission-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-reports/controller/new-vendors-detail-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-reports/controller/title-company-report-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-reports/controller/new-lead-conversion-report-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-reports/controller/end-day-report-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/calendar/controller/calendar-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/staff-user-account-info/controller/staff-user-account-info-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/admin-agent-billing-log/controller/admin-agent-billing-log-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/admin-agent-onboarding-package/controller/admin-agent-onboarding-package-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/disbursement/controller/disbursement-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/add-transactions/controller/add-transactions-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/see-transactions/controller/see-transactions-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-trust-account-deposits/controller/new-trust-account-deposits-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-sky-slope-integration/controller/new-sky-slope-integration-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-dotloop-integration/controller/new-dotloop-integration-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-add-agent/controller/new-add-agent-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-payment-log/controller/new-payment-log-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/export-data/controller/export-data-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-agent-1099s/controller/new-agent-1099s-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-pay-vendors/controller/new-pay-vendors-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-agent-billing/controller/new-agent-billing-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-broker-account-info/controller/new-broker-account-info-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-staff-users/controller/new-staff-users-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-office-documents/controller/new-office-documents-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/reports-new/controller/new-reports-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-manage-tags/controller/new-manage-tags-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-vendors/controller/new-vendors-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-title-company/controller/new-title-company-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-bank-accounts/controller/new-bank-accounts-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-task-checklist/controller/new-task-checklist-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-direct-deposit/controller/new-direct-deposit-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-credit-card-setting/controller/new-credit-card-setting-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-commission-plan/controller/new-commission-plan-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-integrations/controller/new-integrations-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-plaid-transaction/controller/new-plaid-transaction-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-view-plaid-transaction/controller/new-view-plaid-transaction-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-add-plaid-transaction/controller/new-add-plaid-transaction-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/add-agent-new/controller/add-agent-new-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-broker-template/controller/new-broker-template-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-view-agent/controller/new-view-agent-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-agent-onboarding/controller/new-agent-onboarding-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-agent-commission/controller/new-agent-commission-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-agent-account/controller/new-agent-account-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-agent-documents/controller/new-agent-documents-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-agent-billing-log/controller/new-agent-billing-log-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-pay-agent-confirmation/controller/new-pay-agent-confirmation-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-setup-checklist/controller/new-setup-checklist-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-calendar/controller/new-calendar-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-title-company-report/controller/new-title-company-report-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-agent-monthly-report/controller/new-agent-monthly-report-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-agent-overview-report/controller/new-agent-overview-report-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-cap-plan-report/controller/new-cap-plan-report-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-cap-report/controller/new-cap-report-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-agent-export-report/controller/new-agent-export-report-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-transaction-export-report/controller/new-transaction-export-report-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-closing-fee-report/controller/new-closing-fee-report-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-agent-billings-report/controller/new-agent-billings-report-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-reconciliation-report/controller/brand-new-reconciliation-report-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-transaction-detail-report/controller/new-transaction-detail-report-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-lead-conversion-report/controller/brand-new-lead-conversion-report-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-vendors-detail/controller/brand-new-vendors-detail-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-office-commission-report/controller/brand-new-office-commission-report-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-agent-value-report/controller/new-agent-value-report-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-agent-commission-report/controller/new-agent-commission-report-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-plaid-transaction-detail-report/controller/new-plaid-transaction-detail-report-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-commission-detail-report/controller/new-commission-detail-report-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-profit-and-loss/controller/new-profit-and-loss-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/teams/controller/teams-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/other-income/controller/other-income-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/auto-fill-items/controller/auto-fill-items-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/admin-auto-fill/controller/admin-auto-fill-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/dotloop-company-integration/controller/dotloop-company-integration-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/admin-user-search/controller/admin-user-search-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/admin-dwolla-clear-tokens/controller/admin-dwolla-clear-tokens-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/admin-edit-transactions/controller/admin-edit-transactions-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/admin-company-report/controller/admin-company-report-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/docusign-log/controller/docusign-log-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/admin-reset-onboard/controller/admin-reset-onboard-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/admin-update-users-email/controller/admin-update-users-email-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/admin-undelete-agent/controller/admin-undelete-agent-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/admin-add-start-point/controller/admin-add-start-point-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/admin-mls-db/controller/admin-mls-db-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/mls-integration/controller/mls-integration-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/other-income-log/controller/other-income-log-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/other-income-log/controller/copy-other-income-log-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/dwolla/add-dwolla-customer/controller/add-dwolla-customer-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/dwolla/add-dwolla-receive-only-customer/controller/add-dwolla-receive-only-customer-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/dwolla/add-dwolla-customer-funding-source/controller/add-dwolla-customer-funding-source-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/dwolla/dwolla-pay-vendors/controller/dwolla-pay-vendors-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/dwolla/dwolla-pay-agent-confirmation/controller/dwolla-pay-agent-confirmation-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/admin-reports/controller/admin-reports-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/admin-company-signup-report/controller/admin-company-signup-report-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/docusign-transaction-log/controller/docusign-transaction-log-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-revenue-report/controller/new-revenue-report-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/company-conversion-report/controller/company-conversion-report-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/dwolla/dwolla-transfers-log/controller/dwolla-transfers-log-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/dwolla/dwolla-migrate-customer/controller/dwolla-migrate-customer-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/dwolla/delete-dwolla-customer/controller/delete-dwolla-customer-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/add-vendor/controller/add-vendor-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/admin-undelete-transaction/controller/admin-undelete-transaction-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/vendor-list/controller/vendor-list-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/vendor-account-info/controller/vendor-account-info-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/agent-commission-plan/controller/agent-commission-plan-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/sales-summary-report/controller/sales-summary-report-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/admin-add-company/controller/admin-add-company-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/dwolla/dwolla-activity-log/controller/dwolla-activity-log-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/dwolla/dwolla-log/controller/dwolla-log-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/dwolla/dwolla-webhook-log/controller/dwolla-webhook-log-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/admin-new-dwolla-functionality/controller/admin-new-dwolla-functionality-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/auto-fill-co-broke/controller/auto-fill-co-broke-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/admin-dwolla-webhook-list/controller/admin-dwolla-webhook-list-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/default-state/controller/default-state-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-transactions/controller/copy-add-demand-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/admin-dwolla-save-funding-source-log-list/controller/admin-dwolla-save-funding-source-log-list-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/agent-invoice-billing/controller/agent-invoice-billing-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/import-multi-agent-transaction/controller/import-multi-agent-transaction-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/admin-delete-transactions/controller/admin-delete-transactions-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/pay-staff/controller/pay-staff-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/office-commission-report-revisions/controller/office-commission-report-revisions-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/property-and-lenders-conversion-report/controller/property-and-lenders-conversion-report-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/admin-delete-other-income/controller/admin-delete-other-income-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/admin-delete-agent-credit-entry/controller/admin-delete-agent-credit-entry-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/admin-mls-transactions-view/controller/admin-mls-transactions-view-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/import-billing/controller/import-billing-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/add-report-data/controller/admin-add-report-data-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/admin-sync-transaction/controller/admin-sync-transaction-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/admin-auto-switch-commission-plan/controller/admin-auto-switch-commission-plan-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/admin-edit-account-info/controller/admin-edit-account-info-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/zoho-reports-bulk-sync/controller/zoho-reports-bulk-sync-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/admin-turn-on-feature/controller/admin-turn-on-feature-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/admin-update-commission-plan/controller/admin-update-commission-plan-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/agent-sky-slope-logs/controller/agent-sky-slope-logs-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/admin-activate-agents/controller/admin-activate-agents-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/admin-import-co-broke/controller/admin-import-co-broke-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/admin-copy-agents/controller/admin-copy-agents-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/agent-profile-master-data/controller/agent-profile-master-data-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/admin-certify-beneficial-owner/controller/admin-certify-beneficial-owner-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/admin-charge-broker-by-date/controller/admin-charge-broker-by-date-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/cap-on-fees-report/controller/cap-on-fees-report-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/agent-dotloop-logs/controller/agent-dotloop-logs-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/manage-agent-office-license/controller/manage-agent-office-license-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/agent-billing-status/controller/agent-billing-status-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/agent-billing-overview-report/controller/agent-billing-overview-report-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/edit-title-attorneys/controller/edit-title-attorneys-company-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/edit-title-attorneys/controller/master-mortgage-company-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/edit-title-attorneys/controller/master-escrow-company-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/edit-title-attorneys/controller/internal-company-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/dynamics-export/controller/dynamics-export-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/two-factor-auth/two-factor-auth-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/import-attorneys/controller/import-attorneys-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/zoho-bulk-sync/controller/zoho-bulk-sync-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/broker-permissions/controller/broker-permission-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/agent-view/controller/agent-view-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/admin-agent-credit-card-expiry-email/controller/admin-agent-credit-card-expiry-email-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/copy-agents/controller/copy-agents-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/cap-commission-start-point/controller/cap-commission-start-point-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/commission-plan-fees/controller/commission-plan-fees-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-reports/controller/overbase-commission-report-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-transactions/controller/pre-demand-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/agci-levels/controller/agci-level-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/admin-import-agent-data/controller/admin-import-agent-data-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/import-agent-first-team-custom-fields/controller/import-agent-first-team-custom-fields-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/import-vendors/controller/import-vendors-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/import-title-company/controller/import-title-company-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/field-validation/controller/field-validation-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/import-attorneys/controller/admin-company-import-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/realogy-integration/controller/realogy-integration-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/transaction-master-data/controller/transaction-master-data-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/edit-title-attorneys/controller/financial-partners-controller.js");--}}

{{--    loadScriptFile("js/custom/angularjs/admin-sales-tax-report/controller/admin-sales-tax-report-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/admin-nexthome-revenue-report/controller/admin-nexthome-revenue-report-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/admin-ez-coordinator-affiliate-report/controller/admin-ezcoordinator-affiliate-report-controller.js");--}}

{{--    loadScriptFile("js/custom/angularjs/escrow-company-report/controller/escrow-company-report-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/edit-title-attorneys/controller/master-insurance-company-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/admin-weichert/controller/admin-weichert-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/agent-billings-setup/controller/agent-billings-setup-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-reports/controller/shortage-report-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/admin-migrate-data/controller/admin-migrate-company-data-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/admin-add-next-qb-txn-no/controller/save-update-next-qb-txn-no-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/broker-account-info/controller/broker-customize-signature-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/admin-estimated-delivery-date-updation/controller/admin-estimated-delivery-date-updation-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/agent-fees-report/controller/agent-fees-report-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/admin-agent-signed-document/controller/admin-agent-signed-document-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/admin-realogy/controller/admin-realogy-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/upgrade-downgrade-report/controller/upgrade-downgrade-report-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/agent-portal-checklist/controller/agent-portal-checklist-controller.js");--}}
{{--</script>--}}

{{--<!-- Test reports -->--}}
{{--<script type="text/javascript">--}}
{{--    loadScriptFile("js/custom/angularjs/new-reports/controller/test-agent-commission-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-reports/controller/test-agent-value-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-reports/controller/test-transaction-detail-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-reports/controller/test-commission-detail-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-reports/controller/agent-billings-report-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-reports/controller/rollover-date-report-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-reports/controller/transaction-export-report-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-reports/controller/agent-export-report-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-reports/controller/closing-fee-report-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-reports/controller/agent-monthly-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-reports/controller/agent-overview-report-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-reports/controller/cap-plan-report-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-reports/controller/hotsheet-report-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-reports/controller/commission-cutting-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-reports/controller/agent-1099s-report-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-reports/controller/post-commission-report-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/reports/agent-commission-report/controller/agent-commission-report-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/reports/commission-detail-report/controller/commission-detail-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/reports/agent-lead-conversion-report/controller/agent-lead-conversion-report-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/reports/pipeline-report/controller/pipeline-report-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/reports/office-commission-breakdown/controller/office-commission-breakdown-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/reports/office-commission-breakdown/controller/copy-office-commission-breakdown-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-export-data/controller/new-export-data-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/vendors/controller/vendors-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/edit-title-attorneys/controller/title-company-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/dotloop-integration/controller/dotloop-integration-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/sky-slope-integration/controller/sky-slope-integration-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/sky-slope-integration/controller/broker-sky-slope-log-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/kvcore-integration/controller/kvcore-integration-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/plaid/controller/plaid-dashboard-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/plaid/controller/plaid-transaction-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/plaid/controller/add-plaid-transaction-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/plaid/controller/pay-vendors-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/plaid/controller/manage-tags-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/plaid/controller/profit-and-loss-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/plaid/controller/bank-accounts-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/plaid/controller/view-plaid-transaction-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/plaid/controller/plaid-billing-info-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/plaid/controller/plaid-transaction-detail-report-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/plaid/controller/plaid-agent-account-info-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/plaid/service/plaid-service.js");--}}
{{--    loadScriptFile("js/custom/angularjs/directive/common-directives.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-transactions/directive/directive.js");--}}
{{--    loadScriptFile("js/custom/angularjs/view-agent/directive/directive.js");--}}
{{--    loadScriptFile("js/custom/angularjs/directive/brokersumo-directive.js");--}}
{{--    loadScriptFile("js/custom/angularjs/agent-billing/controller/agent-billing-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/task-checklist/service/task-checklist-service.js");--}}
{{--    loadScriptFile("js/custom/angularjs/task-checklist/controller/task-checklist-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/agent-checklist/service/agent-checklist-service.js");--}}
{{--    loadScriptFile("js/custom/angularjs/agent-checklist/controller/agent-checklist-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/admin-skyslope/controller/admin-skyslope-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/import-transaction/controller/import-transaction-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/export-transaction/controller/export-transaction-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/admin-add-billing-item/controller/admin-add-billing-item-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/broker-fee/controller/broker-fee-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/payment-log/controller/payment-log-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/credit-card-info/controller/credit-card-info-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/add-cap/controller/add-cap-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/forgot-password-link/controller/forgot-password-link-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/docusign-template/controller/docusign-template-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/admin-agent-activity-log/controller/admin-agent-activity-log-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/admin-add-license-types/controller/admin-add-license-types-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/organization/controller/organization-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/organization/controller/region-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/organization/controller/office-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/add-license-types/controller/add-license-types-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/onboarding-queue/controller/onboarding-queue-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/organization/controller/company-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/bank-deposit/controller/bank-deposit-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-reports/controller/sliding-scale-plan-report-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/organization/controller/division-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/stay-agreement-report/controller/stay-agreement-report-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/transaction-master-data/controller/transaction-master-data-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/edit-title-attorneys/controller/financial-partners-controller.js");--}}
{{--</script>--}}
{{--<!-- custom angular js files starts here -->--}}
{{--<script type="text/javascript">loadScriptFile("js/external/FileSaver/FileSaver.js")</script>--}}

{{--<!-- custom angular js files ends here -->--}}

{{--<!-- start directive js -->--}}

{{--<script type="text/javascript">--}}
{{--    loadScriptFile("js/custom/angularjs/disbursement/directives/disbursment-directives.js");--}}
{{--    loadScriptFile("js/custom/angularjs/add-transactions/directives/transactions-directives.js");--}}
{{--    loadScriptFile("js/custom/angularjs/plaid-directives/directives/plaid-directives.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-view-agent/directives/new-view-agent-directives.js");--}}
{{--    loadScriptFile("js/custom/angularjs/teams/directive/teams-directive.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-add-agent/directives/directive.js");--}}
{{--    loadScriptFile("js/custom/angularjs/reports/directive/reports-modal-directive.js");--}}
{{--    loadScriptFile("js/custom/angularjs/organization/directive/organization-directive.js");--}}
{{--    loadScriptFile("js/custom/angularjs/agent-profile-master-data/directive/agent-profile-master-data-directive.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-transactions/controller/view-transaction-escrow-company-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-transactions/controller/view-transaction-mortgage-company-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-transactions/controller/first-team-info-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-transactions/controller/weichert-fee-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/transaction-master-data/directive/transaction-master-data-directive.js");--}}
{{--    loadScriptFile("js/custom/angularjs/edit-title-attorneys/directive/directive.js");--}}
{{--    loadScriptFile("js/custom/angularjs/commission-plan-fees/directive/directive.js");--}}
{{--    loadScriptFile("js/custom/angularjs/agent-view/directive/agent-view-directives.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-transactions/controller/view-transaction-company-dollar-portion-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/new-transactions/controller/view-transaction-insurance-company-controller.js");--}}
{{--</script>--}}

{{--<!-- end directive js -->--}}

{{--<!-- start directive controller -->--}}

{{--<script type="text/javascript">--}}
{{--    loadScriptFile("js/custom/angularjs/organization/controller/view-region-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/organization/controller/view-offices-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/organization/controller/license-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/organization/controller/view-company-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/organization/controller/view-division-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/organization/controller/commission-plan-assigned-to-office-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/organization/controller/commission-plan-assigned-to-region-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/agent-profile-master-data/controller/add-edit-agent-types-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/agent-profile-master-data/controller/add-edit-agent-license-status-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/agent-profile-master-data/controller/add-edit-agent-experience-level-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/agent-profile-master-data/controller/add-edit-agent-source-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/organization/controller/view-company-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/agent-profile-master-data/controller/add-edit-company-brand-x-companies-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/agent-profile-master-data/controller/add-edit-termination-reason-code-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/commission-plan-fees/controller/commission-plan-region-fees-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/commission-plan-fees/controller/commission-plan-office-fees-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/commission-plan-fees/controller/commission-plan-state-fees-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/transaction-master-data/controller/add-edit-transaction-sub-type-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/transaction-master-data/controller/add-edit-warranty-plan-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/transaction-master-data/controller/add-edit-hazard-disclosure-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/transaction-master-data/controller/add-edit-loan-rep-controller.js");--}}
{{--</script>--}}

{{--<!-- Agent View  Directive -->--}}
{{--<script type="text/javascript">--}}
{{--    loadScriptFile("js/custom/angularjs/agent-view/controller/agent-view-profile-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/agent-view/controller/agent-view-document-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/agent-view/controller/agent-view-billing-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/agent-view/controller/agent-view-credit-card-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/agent-view/controller/agent-view-commission-log-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/agent-view/controller/agent-view-permission-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/agent-view/controller/agent-view-plan-and-fees-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/agent-view/controller/agent-view-notes-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/agent-view/controller/agent-view-billing-log-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/agent-view/controller/agent-view-task-check-list-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/agent-view/controller/agent-view-payment-log-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/agent-view/controller/agent-view-other-income-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/agent-view/controller/agent-view-custom-profile-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/agent-view/controller/agent-view-agents-recruited-controller.js");--}}
{{--    loadScriptFile("js/custom/angularjs/agent-view/controller/agent-view-history-controller.js");--}}
{{--</script>--}}

{{--<!-- Agent View  Directive End-->--}}

{{--<!-- end directive controller -->--}}

{{--<!-- table to excel converter -->--}}
{{--<script type="text/javascript">loadScriptFile("js/external/jquery-table2excel-master/src/jquery.table2excel.js")</script>--}}

{{--<script type="text/javascript">loadScriptFile("js/xlsx.core.min.js")</script>--}}

{{--<!-- sortable -->--}}
{{--<script type="text/javascript">loadScriptFile("js/external/sortable/sortable.js")</script>--}}

{{--<!-- Dwolla -->--}}
{{--<script type="text/javascript">loadScriptFile("js/external/dwolla/dwolla.js")</script>--}}

{{--<!-- color picker -->--}}
{{--<script type="text/javascript">loadScriptFile("js/external/color-picker/color-picker.min.js")</script>--}}

{{--<!-- mentio -->--}}
{{--<script type="text/javascript">loadScriptFile("js/external/mentio/mentio.js")</script>--}}

{{--<script type="text/javascript">--}}
{{--    $(document).ready(function () {--}}

{{--        'use strict';--}}
{{--        $("#postalCode").keypress(function (e) {--}}
{{--            if (e.which != 8 && e.which != 45 && e.which != 0 && (e.which < 48 || e.which > 57)) {--}}
{{--                return false;--}}
{{--            }--}}
{{--        });--}}

{{--        $('form').validator({--}}
{{--            disable:false--}}
{{--        });--}}

{{--        $(".alert .close").on('click',--}}
{{--            function showAlert(e) {--}}
{{--                e.stopPropagation();--}}
{{--                $("#success-alert").alert(100);--}}
{{--                $("#success-alert").fadeTo(200, 100).slideUp(500, function(){--}}
{{--                    $("#success-alert").alert('close');--}}
{{--                });--}}
{{--            });--}}


{{--    });--}}
{{--    $(window).load(function() {--}}
{{--        windowSize();--}}
{{--        windowResize();--}}
{{--    });--}}
{{--    function windowSize(){--}}

{{--        var windowHeight = $(window).height();--}}
{{--        var sideMenuHeight = $('.main-sidebar').height();--}}
{{--        if(sideMenuHeight > windowHeight){--}}
{{--            windowHeight = sideMenuHeight+48;--}}
{{--        }--}}
{{--        if($(window).width() > 767){--}}
{{--            setTimeout(function(){--}}
{{--                $(".content-wrapper").css('min-height', windowHeight-($(".main-footer").height()+31+$(".main-header").height())+"px");--}}
{{--            },200);--}}
{{--        }--}}
{{--        else{--}}
{{--            setTimeout(function(){--}}
{{--                $(".content-wrapper").css('min-height', windowHeight-($(".main-footer").height()+39+$(".main-header").height())+"px");--}}
{{--            },200);--}}

{{--        }--}}
{{--    }--}}

{{--    function windowResize(){--}}
{{--        $(window).resize(function() {--}}
{{--            windowSize();--}}
{{--        });--}}
{{--    }--}}

{{--    function sideBarMenuItems(id){--}}
{{--        $('.skin-red .sidebar-menu li').removeClass('active');--}}
{{--        $('.skin-red .sidebar-menu li .treeview-menu li a').removeClass("active-color");--}}
{{--        $("#"+id).addClass("active-color");--}}
{{--        $("#"+id).parent(".treeview").addClass("active");--}}
{{--    }--}}
{{--</script>--}}

{{--<script>--}}
{{--    function interComFunc() {--}}
{{--        var w = window;--}}
{{--        var ic = w.Intercom;--}}
{{--        if (typeof ic === "function") {--}}
{{--            ic('reattach_activator');--}}
{{--            ic('update', intercomSettings);--}}
{{--        } else {--}}
{{--            var d = document;--}}
{{--            var i = function() {--}}
{{--                i.c(arguments)--}}
{{--            };--}}
{{--            i.q = [];--}}
{{--            i.c = function(args) {--}}
{{--                i.q.push(args)--}}
{{--            };--}}
{{--            w.Intercom = i;--}}

{{--            function l() {--}}
{{--                var s = d.createElement('script');--}}
{{--                s.type = 'text/javascript';--}}
{{--                s.async = true;--}}
{{--                s.src = 'https://widget.intercom.io/widget/wk7giaoj';--}}
{{--                var x = d.getElementsByTagName('script')[0];--}}
{{--                x.parentNode.insertBefore(s, x);--}}
{{--            };--}}
{{--            l();--}}
{{--        }--}}
{{--    };--}}
{{--</script>--}}
{{--<input class="hide" data-ng-model="user.emailId" type="text" id ="loggedInUserEmail">--}}
{{--<script type="text/javascript">--}}
{{--    setTimeout(function(){--}}
{{--        var userEmail = $('#loggedInUserEmail').val();--}}
{{--        !function(e,t,r,n,a){if(!e[a]){for(var i=e[a]=[],s=0;s<r.length;s++){var c=r[s];i[c]=i[c]||function(e){return function(){var t=Array.prototype.slice.call(arguments);i.push([e,t])}}(c)}i.SNIPPET_VERSION="1.0.1";var o=t.createElement("script");o.type="text/javascript",o.async=!0,o.src="https://d2yyd1h5u9mauk.cloudfront.net/integrations/web/v1/library/"+n+"/"+a+".js";var p=t.getElementsByTagName("script")[0];p.parentNode.insertBefore(o,p)}}(window,document,["survey","reset","config","init","set","get","event","identify","track","page","screen","group","alias"],"4R1nH721T4PsJNwE","delighted");--}}

{{--        delighted.survey({--}}
{{--            email: userEmail, // customer email (optional)--}}
{{--            properties: { questionProductName: "BrokerSumo", },--}}
{{--        });--}}
{{--    }, 5000);--}}
{{--</script>--}}

{{--<!----}}
{{--    <script type="text/javascript">--}}
{{--    	var version = "2020-10-29";--}}
{{--		var sc = "<script type="+"'text/javascript'" + "src='js/custom/angularjs/agent-view/controller/agent-view-history-controller.js?ver="+version+"'" + ">"+"</"+"script>";--}}
{{--		document.write(sc);--}}
{{--	    //document.getElementById("myText").innerHTML = versionUpdate;--}}
{{--	</script>--}}
{{---->--}}


{{--</body>--}}
{{--</html>--}}
