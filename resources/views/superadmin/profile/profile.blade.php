@extends('superadmin.layouts.master')
@section('title')
    Profile
@endsection
@push('css')
@endpush
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="mt-4">
                <h5 class="font-size-14 mb-4"><i class="mdi mdi-arrow-right text-primary me-1"></i>Profile</h5>
                @if(session()->has('success_message'))
                    <div class="alert alert-success" style="width:100%;">
                        {{session()->get('success_message')}}
                    </div>
                @endif
                <form action="{{route('updateProfile')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label class="form-label" for="formrow-firstname-input">Username</label>
                                <input type="text" name="username" value="{{\Auth::guard('admin')->user()->username}}" class="form-control" id="formrow-firstname-input">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label class="form-label" for="formrow-email-input">Image</label>
                                <input type="file" class="form-control" name="image" id="formrow-email-input">
                                <img src="/{{\Auth::guard('admin')->user()->image}}" onerror="this.src='/uploads/dummy.png'" style="width:150px;height:100px;" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label class="form-label" for="formrow-password-input">Password</label>
                                <input type="password" class="form-control" name="password" id="formrow-email-input">
                            </div>
                        </div>
                    </div>
                    <div class="mt-4">
                        <button type="submit" class="btn btn-primary w-md">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('js')

@endpush
