@extends('superadmin.layouts.master')

@push('css')
    <style>
    </style>
@endpush

@section('content')
    <style class="ng-scope">
        .fa-spin {
            animation: 2s linear 0s normal none infinite running fa-spin !important;
        }
        .isComplete {
            color: green;
        }
        .isIncomplete {
            color: red;
        }
    </style>
    <section class="content-header">
        <ol class="breadcrumb breadcrumb_lg">
            <li class="active">Setup Checklist</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box_custom box-primary">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table_wrapper agent_table table-responsive">
                                <div class="loading">
                                    <i class="fa fa-refresh"></i>
                                </div>
                                <table id="agent_table" class="table table_hover">
                                    <tbody>
                                    <tr>
                                        <td>Complete Your Account Settings (required)</td>
                                        <td>
                                            <label class="isIncomplete ng-scope" >Incomplete</label>
                                        </td>
                                        <td>
                                            <a href="#" class="ng-scope">Setup Now</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Contact Sales to Setup a 1-on-1 Go Live Call - email <a href="mailto:cbosales@insiderealestate.com" target="_blank">cbosales@insiderealestate.com</a> or call (801) 446-6482</td>
                                        <td>
                                            <label class="isIncomplete ng-scope">Incomplete</label>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Add a Commission Plan (required)</td>
                                        <td><label data-ng-show="commissionPlanStatus" class="isComplete">Complete</label>
                                            <label class="isIncomplete ng-hide" data-ng-hide="commissionPlanStatus">Incomplete</label>
                                        </td>
                                        <td>
                                            <a href="#" id="commission-plan" data-ng-hide="commissionPlanStatus" class="ng-hide">Setup Now</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Create an Onboarding Template</td>
                                        <td>
                                            <label data-ng-show="onboardingTemplateStatus" class="isComplete ng-hide">Complete</label>
                                            <label class="isIncomplete" data-ng-hide="onboardingTemplateStatus">Incomplete</label>
                                        </td>
                                        <td>
                                            <a href="#" data-ng-hide="onboardingTemplateStatus">Setup Now</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Get Approved For Agent Billings</td>
                                        <td>
                                            <label class="isComplete ng-hide">Complete</label>
                                            <label class="isIncomplete">Incomplete</label>
                                        </td>
                                        <td>
                                            <a href="#" >Setup Now</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Add Staff Users</td>
                                        <td>
                                            <label data-ng-show="getStaffUserStatus" class="isComplete ng-hide">Complete</label>
                                            <label class="isIncomplete" data-ng-hide="getStaffUserStatus">Incomplete</label>
                                        </td>
                                        <td>
                                            <a href="#" data-ng-hide="getStaffUserStatus">Setup Now</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Add Your Offices</td>
                                        <td>
                                            <label data-ng-show="officeLocationStatus" class="isComplete ng-hide">Complete</label>
                                            <label class="isIncomplete" data-ng-hide="officeLocationStatus">Incomplete</label>
                                        </td>
                                        <td>
                                            <a href="#" data-ng-hide="officeLocationStatus">Setup Now</a>
                                        </td>
                                    </tr>
                                    <tr data-ng-show="user.isPlaidBroker" class="ng-hide">
                                        <td>Add Bank Accounts</td>
                                        <td>
                                            <label data-ng-show="isBankAccountAdded" class="isComplete ng-hide">Complete</label>
                                            <label class="isIncomplete" data-ng-show="!isBankAccountAdded">Incomplete</label>
                                        </td>
                                        <td>
                                            <a href="/#/bank-accounts" data-ng-show="!isBankAccountAdded">Setup Now</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Add Agents</td>
                                        <td>
                                            <label class="isComplete">Complete</label>
                                            <label class="isIncomplete ng-hide">Incomplete</label>
                                        </td>
                                        <td>
                                            <a class="ng-hide">Setup Now</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Add Dotloop Integrations</td>
                                        <td>
                                            <label class="isComplete ng-hide">Complete</label>
                                            <label class="isIncomplete">Incomplete</label>
                                        </td>
                                        <td>
                                            <a href="#">Setup Now</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Add Skyslope Integrations</td>
                                        <td>
                                            <label class="isComplete ng-hide">Complete</label>
                                            <label class="isIncomplete">Incomplete</label>
                                        </td>
                                        <td>
                                            <a href="#">Setup Now</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Have Questions? Contact our Team at <a href="mailto:cbosales@insiderealestate.com" target="_blank">cbosales@insiderealestate.com</a> or call (801) 446-6482.</td>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@push('js')
    <script>

    </script>
@endpush
