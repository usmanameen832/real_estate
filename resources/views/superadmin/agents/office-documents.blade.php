@extends('superadmin.layouts.master')

@push('css')
    <style>
        .card{
            border-top: 3px solid #D5D5D5;
        }
        .apply{
            background-color: #387FA9;
            font-size: 13px;
            padding: 10px 30px;
            margin-left: 85%;
        }
    </style>
@endpush

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box d-flex align-items-center justify-content-between">
                <h1 class="mb-0">Agents / Office Documents(s)</h1>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Office Document(s)</h4>
            <p style="margin-top: 1.5rem">These folder(s) will be accessible to each agent to view and download in their agent portal.</p>

            <button type="button" class="btn btn-primary apply mb-4 mt-3">+ Add Folder(s)</button>

            <div class="table-rep-plugin">
                <div class="table-responsive mb-0" data-pattern="priority-columns">
                    <table id="tech-companies-1" class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Folder Name</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <th>zzx sadgsdg</th>
                            <td>$0.00</td>
                            <td>$0.00</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script>

    </script>
@endpush
