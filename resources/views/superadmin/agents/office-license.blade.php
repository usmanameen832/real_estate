@extends('superadmin.layouts.master')

@push('css')
    <style>
        .page-title-box h1{
            font-size: 30px;
        }
        ul.tabs{
            margin: 0px;
            padding: 0px;
            list-style: none;
        }
        ul.tabs li{
            background: none;
            color: #222;
            display: inline-block;
            padding: 12px 40px;
            cursor: pointer;
        }

        ul.tabs li.current{
            background: #fff;
            color: #222;
            border-top: 3px solid #D5D5D5;
            margin-left: 20px;
        }

        .tab-content{
            display: none;
            padding: 15px;
            background-color: #fff;
        }

        .tab-content.current{
            display: inherit;
        }
    </style>
@endpush

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box d-flex align-items-center justify-content-between">
                <h1 class="mb-0">Agents / Offices & License</h1>
            </div>
        </div>
    </div>
    <div class="container">

        <ul class="tabs">
            <li class="tab-link current" data-tab="tab-1">Offices & Licenses</li>
        </ul>

        <div id="tab-1" class="tab-content current">
            <h4>Offices & Licenses</h4>
           <div class="row">
               <div class="col-2">
                   <p class="mt-4" style="font-weight: bold; font-size: 18px">Agent:</p>
               </div>
               <div class="col-4">
                   <form>
                       <div class="form-group">
                           <label for="exampleFormControlSelect1"></label>
                           <select class="form-control">
                               <option>zz xasd asd</option>
                           </select>
                       </div>
                   </form>
               </div>
               <div class="col-6 mt-3 text-end">
                       <button type="button" class="btn btn-primary">Go to Agent Profile</button>
                       <button type="button" class="btn btn-primary">+ Add Office</button>
                       <button type="button" class="btn btn-secondary">Filter</button>
               </div>
           </div>
        </div>

    </div><!-- container -->


@endsection

@push('js')
    <script>
        $(document).ready(function(){

            $('ul.tabs li').click(function(){
                var tab_id = $(this).attr('data-tab');

                $('ul.tabs li').removeClass('current');
                $('.tab-content').removeClass('current');

                $(this).addClass('current');
                $("#"+tab_id).addClass('current');
            })

        })
    </script>
@endpush
