@extends('superadmin.layouts.master')

@push('css')
    <style>

        .btn-md{
            background-color: #387FA9;
            padding-left: 40px !important;
            padding-right: 40px !important;
            font-size: 12px;
            margin-bottom: 20px;
        }

        .apply{
            background-color: #387FA9;
            margin-left: 10px;
            font-size: 13px;
            padding: 10px 30px;
        }

        .reset{
            font-size: 13px;
            padding: 10px 30px;
        }
    </style>
@endpush

@section('content')
    <div class="col-12">
        <div class="row">
            <div class="col-12">
                <div class="page-title-box d-flex align-items-center justify-content-between">
                    <h1 class="mb-0" style="font-size: 30px">Agents / Other Income Log</h1>
                </div>
            </div>
        </div>
        <div class="card ">
            <div class="card-body">
                <h4 class="card-title">Agent Billing Log</h4>
                <button class="btn btn-info btn-md">Download Excel</button>
                <div class="card" style="padding: 20px; background-color: #F5F5F5; border: 1px solid #D5D5D5" >
                    <div class="card-bdy">
                        <div class="mb-3 row">
                            <label for="example-date-input" class="col-md-2 col-form-label">Close Date</label>
                            <div class="col-4">
                                <input class="form-control" type="date" value="2019-08-19" id="example-date-input">
                            </div>
                            <label class="col-2 col-form-label">Select Status:</label>
                            <div class="col-4 ">
                                <select class="form-select">
                                    <option>All Agents</option>
                                    <option>asdasdas</option>
                                </select>
                            </div>
                        </div>
                        <button class="btn btn-info float-end apply">Apply Filters(s)</button>
                        <button class="btn btn-secondary float-end reset">Reset Filter</button>
                    </div>
                </div>

                <table id="datatable" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Agent Name</th>
                        <th>Item Name</th>
                        <th>Income Amount</th>
                        <th>Date</th>
                        <th>Details</th>
                        <th>Status</th>
                        <th>Pay Date</th>
                        <th>Action</th>
                    </tr>
                    </thead>


                    <tbody>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script>

    </script>
@endpush
