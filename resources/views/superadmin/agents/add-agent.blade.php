@extends('superadmin.layouts.master')

@push('css')
    <style>
      h1{
          font-size: 30px !important;
      }
      .card{
          border-top: 3px solid #408EBC;
      }
    </style>
@endpush

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box d-flex align-items-center justify-content-between">
                <h1 class="mb-0">Agents / Add Agents</h1>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-body">
            <div class="col-12">
                <h5>Add Agent: Who Will Fill Out Agent Information?</h5>
                <h6 class="mt-5">Select who will fill out the Agent information:</h6>
            </div>
            <div class="col-12 mt-4 float-end">
                <div class="radio">
                    <label><input type="radio" name="optradio" checked>
                        Agent Will (send onboarding package)
                    </label>
                    <p>
                        Agent(s) will be emailed the link to a webpage containing their onboarding package, where they will be asked for their information.
                        You will be able to customize this boarding package on the next page, before sending to the Agent(s).
                    </p>
                </div>
                <div class="radio mt-4">
                    <label><input type="radio" name="optradio">
                        I Will (add single agent)
                    </label>
                    <p>
                        You will be manually entering information for a single agent in the following steps. Please have the agent information ready.
                    </p>
                </div>
            </div>
        </div>
        <div class="card-footer text-end">
            <button type="button" class="btn btn-secondary btn-lg">Cancel</button>
            <button type="button" class="btn btn-info btn-lg">Continue</button>

        </div>
    </div>
@endsection

@push('js')
@endpush
