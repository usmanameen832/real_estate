@extends('superadmin.layouts.master')

@push('css')
    <style>
        .card{
            border-top: 3px solid #D5D5D5;
        }

        .mb-3{
            margin-top: 10px;
            margin-bottom: 10px;
        }

        h1 {
            font-size: 30px;
        }
    </style>
@endpush

@section('content')
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box d-flex align-items-center justify-content-between">
                <h1 class="mb-0">Agents / View Agents</h1>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Default Datatable</h4>

                    <form>
                        <div class="row">
                            <div class="col-lg-3">
                                <div class="mb-3">
                                    <select class="form-control select2">
                                            <option value="AK">Search by Name</option>
                                            <option value="HI">Search by External Id</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </form>

                    <table id="datatable" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                        <thead>
                        <tr>
                            <th>Agent Name</th>
                            <th>Commission Plan</th>
                            <th>Start Date</th>
                            <th>Location</th>
                            <th>Status</th>
                            <th>Status Change Date</th>
                            <th>Action</th>
                        </tr>
                        </thead>


                        <tbody>
                        <tr>
                            <td><a href="#">zzx sadgsdg</a> </td>
                            <td>test</td>
                            <td>05/21/2021</td>
                            <td></td>
                            <td>Active</td>
                            <td>N/A</td>
                            <td></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->
@endsection

@push('js')
    <script>

    </script>
@endpush
