@extends('superadmin.layouts.master')

@push('css')
    <style>

        .card{
            border-top: 3px solid #D5D5D5;
        }

        h1 {
            font-size: 30px;
        }

        ul.tabs{
            margin: 0px;
            padding: 0px;
            list-style: none;
        }
        ul.tabs li{
            background: none;
            color: #222;
            display: inline-block;
            padding: 10px 15px;
            cursor: pointer;
            border-radius: 2px;
        }

        ul.tabs li.current{
            background: #fff;
            color: #222;
            border-top: 3px solid #D5D5D5;
            margin-left: 20px;
        }

        .tab-content{
            display: none;
            padding: 15px;
        }

        .tab-content.current{
            display: inherit;
        }

        .apply{
            background-color: #387FA9;
            margin-left: 10px;
            font-size: 13px;
            padding: 10px 30px;
        }

        .reset{
            font-size: 13px;
            padding: 10px 30px;
        }
    </style>
@endpush

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box d-flex align-items-center justify-content-between">
                <h1 class="mb-0">Agents / Agent Billing</h1>
            </div>
        </div>
    </div>

    <div class="col-12">

        <ul class="tabs">
            <li class="tab-link current" data-tab="tab-1">Agent Billing</li>
            <li class="tab-link" data-tab="tab-2">Billing Log</li>
            <li class="tab-link" data-tab="tab-3">Card Declined Entries </li>
        </ul>

        <div id="tab-1" class="tab-content current card card-body">

            <h4 class="card-title">Default Datatable</h4>

            <form class="form-inline">
                <div class="row">
                    <div class="form-group col-3 mt-5 mb-4">
                        <input type="password" class="form-control" id="inputPassword2" placeholder="search..">
                    </div>
                    <button type="submit" class="col-1 btn btn-primary mb-4 mt-5">Search</button>
                </div>
            </form>

            <div class="table-rep-plugin">
                <div class="table-responsive mb-0" data-pattern="priority-columns">
                    <table id="tech-companies-1" class="table">
                        <thead>
                        <tr>
                            <th>Company</th>
                            <th>#</th>
                            <th>Agent Name</th>
                            <th>Balance</th>
                            <th>Credit</th>
                            <th>Next Monthly Billing Date</th>
                            <th>Next Quarterly Billing Date</th>
                            <th>Next Bi-Annual Billing Date</th>
                            <th>Next Annual Billing Date</th>
                            <th>Amount</th>
                            <th>Recurring</th>
                            <th>Last Billing Date</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <th>zzx sadgsdg</th>
                            <td>$0.00</td>
                            <td>$0.00</td>
                            <td>N/A</td>
                            <td>N/A</td>
                            <td>N/A</td>
                            <td>N/A</td>
                            <td>$0.00</td>
                            <td>No</td>
                            <td>N/A</td>
                            <td></td>
                        </tr>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
        <div id="tab-2" class="tab-content card ">
            <h4 class="card-title">Agent Billing Log</h4>
            <button class="btn btn-info btn-md" style="background-color: #7AA8D0 !important; margin-left: 70%; padding-right: 60px !important; padding-left: 60px !important;">Email All Unpaid Invoices</button>
            <div class="card-body">
                <div class="card" style="padding: 20px; background-color: #F5F5F5" >
                    <div class="card-bdy">
                        <div class="mb-3 row">
                            <label class="col-2 col-form-label">Select Status:</label>
                            <div class="col-4 ">
                                <select class="form-select">
                                    <option>All</option>
                                    <option>Paid</option>
                                    <option>Unpaid</option>
                                </select>
                            </div>
                            <label class="col-2 col-form-label">Select Status:</label>
                            <div class="col-4 ">
                                <select class="form-select">
                                    <option>All</option>
                                    <option>Paid</option>
                                    <option>Unpaid</option>
                                </select>
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label for="example-date-input" class="col-md-2 col-form-label">Start Date</label>
                            <div class="col-4">
                                <input class="form-control" type="date" value="2019-08-19" id="example-date-input">
                            </div>
                            <label class="col-2 col-form-label">Date Option:</label>
                            <div class="col-4 ">
                                <select class="form-select">
                                    <option>This Month</option>
                                    <option>This Year</option>
                                    <option>Last Month</option>
                                    <option>Last Year</option>
                                </select>
                            </div>
                        </div>
                        <button class="btn btn-info float-end apply">Apply Filters(s)</button>
                        <button class="btn btn-secondary float-end reset">Reset Filter</button>
                    </div>
                </div>

                <table id="datatable" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Billing Date</th>
                        <th>Agent Name</th>
                        <th>Description</th>
                        <th>Amount</th>
                        <th>Status</th>
                        <th>Memo</th>
                        <th>Pay Date</th>
                        <th>Action</th>
                    </tr>
                    </thead>


                    <tbody>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div id="tab-3" class="tab-content card ">
        <h4 class="card-title">Card Declined Entries</h4>
        <form class="form-inline">
            <div class="row">
                <div class="form-group col-3 mt-5 mb-4">
                    <input type="password" class="form-control" id="inputPassword2" placeholder="search..">
                </div>
                <button type="submit" class="col-1 btn btn-primary mb-4 mt-5">Search</button>
            </div>
        </form>
        <div class="card-body">
            <p style="font-weight: bold; font-size: 14px">*These invoices will be removed from this page once they are paid either by credit card or marked as paid.</p>
            <table id="datatable" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Billing Date</th>
                    <th>Agent Name</th>
                    <th>Description</th>
                    <th>Amount</th>
                    <th>Status</th>
                    <th>Memo</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>asd</td>
                    <td>asd</td>
                    <td>asd</td>
                    <td>asd</td>
                    <td>asd</td>
                    <td>asd</td>
                    <td>asd</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

@endsection

@push('js')
    <script>
        $(document).ready(function(){

            $('ul.tabs li').click(function(){
                var tab_id = $(this).attr('data-tab');

                $('ul.tabs li').removeClass('current');
                $('.tab-content').removeClass('current');

                $(this).addClass('current');
                $("#"+tab_id).addClass('current');
            })

        })
    </script>
@endpush
