@extends('superadmin.layouts.master')

@push('css')
    <style>
        .card{
            border-top: 3px solid #D5D5D5;
        }
        h1{
            font-size: 30px;
        }
        .apply{
            margin: 20px 0;
            margin-left: 85%;
            font-size: 13px;
            padding: 10px 60px;
        }
    </style>
@endpush

@section('content')
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box d-flex align-items-center justify-content-between">
                <h1 class="mb-0">Agents / Onboarding Queue</h1>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title" style="font-weight: 500; font-size: 20px; margin-top: 15px">Onboarding Queue</h4>

                    <button class="btn btn-secondary apply">Filter</button>

                    <table id="datatable" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                        <thead>
                        <tr>
                            <th>Agent Name</th>
                            <th>Agent Office</th>
                            <th>Agent Region</th>
                            <th>Agent Onboarding Status</th>
                            <th>Onboarding Completion Date</th>
                            <th>Task List Assigned</th>
                            <th>Task List Assigned To</th>
                        </tr>
                        </thead>


                        <tbody>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->
@endsection

@push('js')
    <script>

    </script>
@endpush
