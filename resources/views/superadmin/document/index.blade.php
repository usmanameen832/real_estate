@extends('superadmin.layouts.master')
@section('title')
    Upload Document
@endsection
@push('css')
@endpush
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="mt-4">
                <h5 class="font-size-14 mb-4"><i class="mdi mdi-arrow-right text-primary me-1"></i>Upload Document</h5>
                @if(session()->has('success_message'))
                    <div class="alert alert-success" style="width:100%;">
                        {{session()->get('success_message')}}
                    </div>
                @endif
                <form action="{{route('postUploadDocument')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label class="form-label" for="formrow-firstname-input">Name</label>
                                <input type="text" name="name" value="" class="form-control" id="formrow-firstname-input">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label class="form-label" for="formrow-email-input">Document</label>
                                <input type="file" accept="application/pdf" class="form-control" name="document" id="formrow-email-input">
                            </div>
                        </div>
                    </div>
                    <div class="mt-4">
                        <button type="submit" class="btn btn-primary w-md">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-lg-12">
            <table class="table table-condensed">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Type</th>
                </tr>
                </thead>
                <tbody>
                @foreach($documents as $docs)
                    <tr>
                        <td>{{$docs->name}}</td>
                        <td>PDF</td>
                        <td><a href="{{route('deleteDocument',[$docs->id])}}" class="btn btn-danger btn-sm">Delete</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@push('js')

@endpush
