
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>@yield('title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="Tomorrow Land" name="description" />
    <meta content="Tomorrow Land" name="author" />
    @include('superadmin.partials.headerScript')
    @stack('css')
</head>


<body>

<!-- <body data-layout="horizontal" data-topbar="colored"> -->

<!-- Begin page -->
<div id="layout-wrapper">
    @include('superadmin.partials.header')
    <!-- ========== Left Sidebar Start ========== -->
    @include('superadmin.partials.vertical_menu')
    <!-- Left Sidebar End -->
    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                @yield('content')
            </div> <!-- container-fluid -->
        </div>
        <!-- End Page-content -->
        @include('superadmin.partials.footer')
    </div>
    <!-- end main content-->

</div>
<!-- END layout-wrapper -->

<!-- Right Sidebar -->
@include('superadmin.partials.right_bar')
<!-- /Right-bar -->


<!-- Right bar overlay-->
<div class="rightbar-overlay"></div>

@include('superadmin.partials.footerScript')
@stack('js')
</body>
</html>
