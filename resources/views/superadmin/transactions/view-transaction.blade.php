@extends('superadmin.layouts.master')

@push('css')
    <style>
        .card{
            border-top: 3px solid #D5D5D5;
        }
        h1{
            font-size: 30px;
        }

        .apply{
            background-color: #387FA9;
            font-size: 13px;
            padding: 10px 30px;
        }
        .Create{
            background-color: #387FA9;
            padding: 10px 15px;
        }
        .reset{
            font-size: 13px;
            padding: 10px 60px;
            margin-left: 30px;
        }
        .col-md-7{
            margin: 30px 0;
            margin-left: 42%;
        }
    </style>
@endpush

@section('content')
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box d-flex align-items-center justify-content-between">
                <h1 class="mb-0">Transactions / View Transactions</h1>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title" style="font-weight: 500; font-size: 20px; margin-top: 15px">Transaction Record</h4>

                    <div class="col-md-7">
                        <div class="row">
                            <div class="col-md-4">
                                <button class="btn btn-secondary reset">Filter</button>
                            </div>
                            <div class="col-md-4">
                                <button class="btn btn-primary Create">+ Create Disbursement</button>
                            </div>
                            <div class="col-md-4">
                                <button class="btn btn-primary apply">+ Add Transactions</button>
                            </div>
                        </div>
                    </div>

                    <table id="datatable" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                        <thead>
                        <tr>
                            <th>Address</th>
                            <th>Close Date</th>
                            <th>Type</th>
                            <th>Agent(s)</th>
                            <th>Sales Price</th>
                            <th>Lead Source</th>
                            <th>Completion Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>


                        <tbody>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->
@endsection

@push('js')
    <script>

    </script>
@endpush
