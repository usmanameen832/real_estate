@extends('superadmin.layouts.master')

@section('title')
    Dashboard | Tomorrow Land - Admin & Dashboard
@endsection

@push('css')
    <style>
        h1{
            font-size: 30px !important;
        }
    </style>
@endpush


@section('content')
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box d-flex align-items-center justify-content-between">
                <h1 class="mb-0">Dashboard</h1>
            </div>
        </div>
    </div>

@endsection

@push('js')
    <script>

    </script>
@endpush
