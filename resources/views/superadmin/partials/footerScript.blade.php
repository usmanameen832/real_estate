<!-- JAVASCRIPT -->
<script src="{{asset('superadmin/js/jquery.min.js')}}"></script>
<script src="{{asset('superadmin/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('superadmin/js/metisMenu.min.js')}}"></script>
<script src="{{asset('superadmin/js/simplebar.min.js')}}"></script>
<script src="{{asset('superadmin/js/waves.min.js')}}"></script>
<script src="{{asset('superadmin/js/jquery.waypoints.min.js')}}"></script>
<script src="{{asset('superadmin/js/jquery.counterup.min.js')}}"></script>
<!-- apexcharts -->
<script src="{{asset('superadmin/js/apexcharts.min.js')}}"></script>
<script src="{{asset('superadmin/js/dashboard.init.js')}}"></script>


<!-- Required datatable js -->
<script src="{{asset('superadmin/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('superadmin/js/dataTables.bootstrap4.min.js')}}"></script>
<!-- Buttons examples -->
<script src="{{asset('superadmin/js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('superadmin/js/buttons.bootstrap4.min.js')}}"></script>

<script src="{{asset('superadmin/js/buttons.html5.min.js')}}"></script>
<script src="{{asset('superadmin/js/buttons.print.min.js')}}"></script>
<script src="{{asset('superadmin/js/buttons.colVis.min.js')}}"></script>

<!-- Responsive examples -->
<script src="{{asset('superadmin/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('superadmin/js/responsive.bootstrap4.min.js')}}"></script>

<!-- Datatable init js -->
<script src="{{asset('superadmin/js/datatables.init.js')}}"></script>


<script src="{{asset('superadmin/js/app.js')}}"></script>
