<div class="vertical-menu">

    <!-- LOGO -->
    <div class="navbar-brand-box">
        <a href="index.html" class="logo logo-dark">
                        <span class="logo-sm">
                            <img src="assets/images/logo-sm.png" alt="" height="22">
                        </span>
            <span class="logo-lg">
                            <img src="assets/images/logo-dark.png" alt="" height="20">
                        </span>
        </a>

        <a href="index.html" class="logo logo-light">
                        <span class="logo-sm">
                            <img src="assets/images/logo-sm.png" alt="" height="22">
                        </span>
            <span class="logo-lg">
                            <img src="assets/images/logo-light.png" alt="" height="20">
                        </span>
        </a>
    </div>

    <button type="button" class="btn btn-sm px-3 font-size-16 header-item waves-effect vertical-menu-btn">
        <i class="fa fa-fw fa-bars"></i>
    </button>

    <div data-simplebar class="sidebar-menu-scroll">

        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <!-- Left Menu Start -->
            <ul class="metismenu list-unstyled" id="side-menu">
                <li class="menu-title">Menu</li>
                <li>
                    <a href="{{url('/superadmin/home')}}">
                        <i class="uil-dashboard"></i>
                        <span>Dashboard</span>
                    </a>
                </li>
                <li>
                    <a href="{{url('/superadmin/checklist')}}">
                        <i class="uil-dashboard"></i>
                        <span>Setup Checklist</span>
                    </a>
                </li>
                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="uil-users-alt"></i>
                        <span>Agents</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="true">
                        <li>
                            <a href="{{ route('agent') }}">Agents(1)</a>
                            <a href="{{ route('add-agent') }}">+Add Agents</a>
                            <a href="{{ route('office-license') }}">+Add Agents Office/License</a>
                            <a href="{{ route('billing') }}">Agent Billing</a>
                            <a href="{{ route('other-income') }}">Other Income Log</a>
                            <a href="{{ route('office-doc') }}">Office Documents</a>
                            <a href="{{ route('onboarding') }}">Onboarding Queue</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="uil-exchange-alt"></i>
                        <span>Transactions</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="true">
                        <li>
                            <a href="{{ route('view') }}">View Transactions</a>
                            <a href="#">Add Transactions</a>
                            <a href="#">Create Disbursement</a>
                            <a href="#">Trust Account/Deposits</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="uil-dollar-alt"></i>
                        <span>Money</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="true">
                        <li>
                            <a href="#">1099 Exports</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#" class="">
                        <i class=" uil-chart-pie-alt"></i>
                        <span>Reports</span>
                    </a>
                </li>
                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="uil-user"></i>
                        <span>My Account</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="true">
                        <li>
                            <a href="#">Account Settings</a>
                            <a href="#">Staff Users</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="uil-cog"></i>
                        <span>Settings</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="true">
                        <li>
                            <a href="#">Agent Billing</a>
                            <a href="#">Agent Checklist</a>
                            <a href="#">Commission Plans</a>
                            <a href="#">Closing Vendors</a>
                            <a href="#">Default State</a>
                            <a href="#">Edit fees</a>
                            <a href="#">Field Validation</a>
                            <a href="#">Integrations</a>
                            <a href="#">Import Agent Billing Data</a>
                            <a href="#">Onboarding Templates</a>
                            <a href="#">Organization</a>
                            <a href="#">Permissions</a>
                            <a href="#">Tags</a>
                            <a href="#">Transaction Checklist</a>
                            <a href="#">Vendors</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class=" uil-question-circle"></i>
                        <span>Help</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="true">
                        <li>
                            <a href="#">Email Support</a>
                            <a href="#">Support Center</a>
                        </li>
                    </ul>
                </li>
                {{--                <li>--}}
                {{--                    <a href="{{route('uploadDocument')}}" class="waves-effect">--}}
                {{--                        <i class="uil-window-section"></i>--}}
                {{--                        <span>Document's</span>--}}
                {{--                    </a>--}}
                {{--                </li>--}}
            </ul>
        </div>
        <!-- Sidebar -->
    </div>
</div>
