@extends('superadmin.layouts.master')
@section('title')
    Blog
@endsection
@push('css')
@endpush
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="mt-4">
                <h5 class="font-size-14 mb-4"><i class="mdi mdi-arrow-right text-primary me-1"></i> Blog Creation</h5>
                @if(session()->has('success_message'))
                    <div class="alert alert-success" style="width:100%;">
                        {{session()->get('success_message')}}
                    </div>
                @endif
                @if(isset($data))
                    <form action="{{route('updateBlog',[$data->id])}}" method="POST" enctype="multipart/form-data">
                @else
                    <form action="{{route('blogPost')}}" method="POST" enctype="multipart/form-data">
                @endif
                    @csrf
                    <div class="row">
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label class="form-label" for="formrow-firstname-input">Name</label>
                                <input type="text" value="{{@$data->name}}" name="name" class="form-control" id="formrow-firstname-input">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label class="form-label" for="formrow-firstname-input">keyword</label>
                                <input type="text" value="{{@$data->keyword}}" name="keyword" class="form-control" id="formrow-firstname-input">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="mb-3">
                                <label class="form-label" for="formrow-email-input">Image</label>
                                <input type="file" class="form-control" name="image" id="formrow-email-input">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="mb-3">
                                <label class="form-label" for="formrow-password-input">Description</label>
                                <textarea name="description" cols="4" rows="4" class="form-control">{{@$data->description}}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="mt-4">
                        @if(isset($data))
                            <button type="submit" class="btn btn-primary w-md">Update</button>
                        @else
                            <button type="submit" class="btn btn-primary w-md">Submit</button>
                        @endif
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('js')

@endpush
