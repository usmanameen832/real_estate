<?php

namespace App\Models;

use App\Traits\General;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    use HasFactory , General;

    protected $fillable = [
       'name' , 'image' , 'description' , 'keyword'
    ];

    public static function createBlogPost($data) {
        $fileName = '';
        if($data['image']){
            $path = 'uploads/blog';
            $fileName = self::uploadFile($path , $data['image']);
        }
        return parent::create([
           'name' => $data['name'],
           'image' => $fileName,
           'description' => $data['description']
        ]);
    }

    public static function updateBlogPost($id , $data) {
        $fileName = '';
        if(isset($data['image'])){
            $path = 'uploads/blog';
            $fileName = self::uploadFile($path , $data['image']);
        }
        return parent::where('id','=',$id)->update([
           'name' => $data['name'],
           'image' => $fileName,
           'description' => $data['description'],
           'keyword' => $data['keyword']
        ]);
    }
}
