<?php

namespace App\Models;

use App\Traits\General;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Auth\Authenticatable as AuthenticableTrait;
use Illuminate\Support\Facades\Auth;

/**
 * @method static Builder where($column, $operator = null, $value = null, $boolean = 'and')
 * @method static Builder create(array $attributes = [])
 */
class Admin extends Model implements Authenticatable
{
    use HasFactory , AuthenticableTrait , General;

    protected $fillable = [
      'username' , 'email' , 'password' , 'image'
    ];

    public static function updateAdminUser($data) {
        if(isset($data['password'])) {
            self::where('id','=',Auth::guard('admin')->user()->id)->update([
                'password' => bcrypt($data['password'])
            ]);
        }
        $image = '';
        if(isset($data['image'])) {
            $path = 'uploads/admin';
            $image = self::uploadFile($path,$data['image']);
        }
        self::where('id','=',Auth::guard('admin')->user()->id)->update([
            'username' => isset($data['username']) ? $data['username'] : Auth::guard('admin')->user()->username,
            'email' => isset($data['email']) ? $data['email'] : Auth::guard('admin')->user()->email,
            'image' => isset($image) ? $image : Auth::guard('admin')->user()->image
        ]);
    }
}
