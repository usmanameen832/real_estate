<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Faq extends Model
{
    use HasFactory;

    protected $fillable = [
        'question' , 'description'
    ];

    public static function createFaq($data) {
        self::create([
           'question' => $data['question'],
           'description' =>  $data['description']
        ]);
    }

    public static function updateFaq($id , $data) {
        self::where('id','=',$id)->update([
           'question' => $data['question'],
           'description' =>  $data['description']
        ]);
    }
}
