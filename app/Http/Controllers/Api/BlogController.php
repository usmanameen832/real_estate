<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Blog;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    public function getBlogs() {
        try {
            $data = Blog::all();
            return collect([
                'status' => true,
                'data' => $data
            ]);
        } catch (\Exception $e) {
            return collect([
                'status' => false,
                'message' => $e->getMessage()
            ]);
        }
    }

    public function getBlogDetail($id) {
        $data = Blog::where('id','=',$id)->first();
        return collect([
           'status' => true,
           'data' => $data
        ]);
    }
}
