<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Http\Controllers\Controller;
use App\Models\Faq;
use Illuminate\Http\Request;

class FaqController extends Controller
{
    public function index() {
        return view('superadmin.faq.create');
    }
    public function allFaq() {
        $data = Faq::all();
        return view('superadmin.faq.list' , [
            'data' => $data
        ]);
    }
    public function faqPost(Request $request) {
        Faq::createFaq($request->all());
        return back()->with('success_message','Faq Created Successfully .. !');
    }
    public function editFaq($id) {
        $data = Faq::where('id','=',$id)->first();
        return view('superadmin.faq.create',[
            'data' => $data
        ]);
    }
    public function deleteFaq($id) {
        Faq::where('id','=',$id)->delete();
        return back()->with('success_message','Faq Deleted Successfully .. !');
    }
    public function updateFaq($id , Request $request) {
        Faq::updateFaq($id , $request->all());
        return back()->with('success_message','Faq Updated Successfully .. !');
    }
}
