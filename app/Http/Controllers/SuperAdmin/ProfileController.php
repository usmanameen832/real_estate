<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function profile() {
        return view('superadmin.profile.profile');
    }

    public function updateProfile(Request $request) {
        Admin::updateAdminUser($request->all());
        return back()->with('success_message','Profile Updated Successfully .... !');
    }
}
