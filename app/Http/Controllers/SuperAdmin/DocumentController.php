<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Http\Controllers\Controller;
use App\Models\Document;
use App\Traits\General;
use Illuminate\Http\Request;

class DocumentController extends Controller
{
    use General;

    public function uploadDocument() {
        $documents = Document::all();
        return view('superadmin.document.index',[
            'documents' => $documents
        ]);
    }

    public function deleteDocument($id) {
        Document::where('id','=',$id)->delete();
        return  back()->with('success_message','File Deleted Successfully ... !');
    }

    public function postUploadDocument(Request $request) {
        $file = $request->document;
        $path = 'uploads/pdf';
        $name = $this->uploadFile($path,$file);
        Document::create([
           'name' => $request->name,
           'document' => $name
        ]);
        return  back()->with('success_message','File Uploaded Successfully ... !');
    }
}
