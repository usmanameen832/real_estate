<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ChecklistController extends Controller
{
    //
    public function checkList(){
        return view('superadmin.checklist.index');
    }
}
