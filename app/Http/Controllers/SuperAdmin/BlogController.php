<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Http\Controllers\Controller;
use App\Models\Blog;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    public function addBlog() {
        return view('superadmin.blog.create');
    }

    public function blogPost(Request $request) {
        Blog::createBlogPost($request->all());
        return back()->with('success_message','Blog Posted Successfully .... !');
    }

    public function deleteBlog($id) {
        Blog::where('id','=',$id)->delete();
        return back()->with('success','Blog Deleted Successfully .. !');
    }

    public function editBlog($id) {
        $data = Blog::where('id','=',$id)->first();
        return view('superadmin.blog.create' , [
            'data' => $data
        ]);
    }

    public function updateBlog($id , Request $request) {
        Blog::updateBlogPost($id , $request->all());
        return back()->with('success_message','Blog Updated Successfully .... !');
    }

    public function allBlogs() {
        $blogs = Blog::all();
        return view('superadmin.blog.list',[
            'blogs' => $blogs
        ]);
    }
}
