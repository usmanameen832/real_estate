<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AgentsController extends Controller
{
    //
    public function agent(){
        return view('superadmin.agents.agent');
    }

    public function addAgent(){
        return view('superadmin.agents.add-agent');
    }

    public function officeLicense(){
        return view('superadmin.agents.office-license');
    }

    public function agentBilling(){
        return view('superadmin.agents.billing');
    }

    public function otherIncome(){
        return view('superadmin.agents.other-income');
    }

    public function officeDoc(){
        return view('superadmin.agents.office-documents');
    }

    public function onboarding(){
        return view('superadmin.agents.onboarding');
    }
}
