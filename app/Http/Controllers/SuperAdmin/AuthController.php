<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function showLoginForm() {
        return view('auth.login');
    }

    public function loginPost(Request $request) {
        $request->validate([
            'email' => 'required',
            'password' => 'required',
        ]);
        if(Auth::guard('admin')->attempt(['email' => $request->email , 'password' => $request->password])) {
            return redirect('/superadmin/home');
        } else {
            return back()->with('error','Credentials Do Not Match ... !');
        }
    }

    public function logoutAdmin() {
        Auth::guard('admin')->logout();
        return redirect('/superadmin/login');
    }
}
