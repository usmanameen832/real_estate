<?php

use Illuminate\Support\Facades\File;


/**
 * create by PhpStorm
 * Syed Wali Shah
 * 07/05/21
 * 4:10 PM
 * */

function getSuccessResponse($status , $data , $user=null) {
    return collect([
        'status' => $status,
        'data' => $data,
        'user' => $user
    ]);
}

function getErrorResponse($status , $message) {
    return collect([
        'status' => $status,
        'message' => $message
    ]);
}




