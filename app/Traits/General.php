<?php


namespace App\Traits;


use Illuminate\Support\Facades\Auth;

trait General
{
    public static function uploadFile($path,$file){
        if($file) {
            $filename = $file->getClientOriginalName();
            $file->move($path,$filename);
            return $path.'/'.$filename;
        }
    }
}
